//
//  UserService.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RxSwift

class UserService: UserStoreProtocol {
  
  func loginUser(parameters: Parameters) -> Observable<ResponseLogin> {
    
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(UserRouter.userLogin(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseLogin>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func employeeLogin(parameters: Parameters) -> Observable<ResponseEmployeeLogin> {
    
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(UserRouter.employeeLogin(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseEmployeeLogin>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
}
