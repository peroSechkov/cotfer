//
//  AccessTokenAdapter.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

class AccessTokenAdapter: RequestAdapter {
  private let accessToken: String
  
  init(accessToken: String) {
    self.accessToken = accessToken
  }
  
  func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
    var urlRequest = urlRequest
    
    if let urlString = urlRequest.url?.absoluteString, urlString.hasPrefix(BASE_API_URL) {
      urlRequest.setValue("Bearer " + accessToken, forHTTPHeaderField: "Authorization")
    }
    urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    return urlRequest
  }
}

