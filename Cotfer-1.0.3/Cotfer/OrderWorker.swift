//
//  OrderWorker.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import CoreStore

class OrderWorker
{
    var orderStore: OrderStoreProtocol
    
    init(orderStore: OrderStoreProtocol) {
        self.orderStore = orderStore
    }
    
    func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return orderStore.addOrder(parameters: parameters)
    }
    
    func getOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return orderStore.getOrder(parameters: parameters)
    }
    
    func updateOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return orderStore.updateOrder(parameters: parameters)
    }
    
    func checkoutOrder(parameters: Parameters) -> Observable<ResponseCheckout> {
        return orderStore.checkoutOrder(parameters: parameters)
    }
    
    func deleteOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return orderStore.deleteOrder(parameters: parameters)
    }
    
    func fetchStoredOrders(_ completionHandler: @escaping (_ orders: [ManagedOrder], _ error: OrdersOperationError?) -> Void){
        orderStore.fetchStoredOrders { (orders: [ManagedOrder], error) -> Void in
            completionHandler(orders, error)
        }
    }
    
    func fetchOrder(_ id: String, completionHandler: @escaping (_ order: ResponseCartDetails?, _ error: OrdersOperationError?) -> Void) {
        orderStore.fetchOrder(id) {(order, error) -> Void in
            completionHandler(order, error)
        }
    }
    
    func storeOrder(_ orderToStore: ResponseCartDetails, completionHandler: @escaping (_ error: OrdersOperationError?) -> Void) {
        orderStore.storeOrder(orderToStore) { error in
            completionHandler(error)
        }
    }
    
    func updateOrder(order: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        orderStore.updateOrder(order: order, completionHandler: completionHandler)
    }
    
    func deleteOrderFromCoreData(orders: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        orderStore.deleteOrders(order: orders) { error in
            completionHandler(error)
        }
    }
    
    func deleteSpecificOrderFromCoreData(orderId : String,completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        orderStore.deleteSpecificOrderFromCoreData(orderId: orderId) { error in
            completionHandler(error)
        }
    }
}


protocol OrderStoreProtocol {
  func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList>
  func getOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList>
  func updateOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList>
  func checkoutOrder(parameters: Parameters) -> Observable<ResponseCheckout>
  func deleteOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList>
  func fetchStoredOrders(_ completionHandler: @escaping (_ orders: [ManagedOrder], _ error: OrdersOperationError?) -> Void)
  func fetchOrder(_ id: String, completionHandler: @escaping (_ order: ResponseCartDetails?, _ error: OrdersOperationError?) -> Void)
  func storeOrder(_ orderToStore: ResponseCartDetails, completionHandler: @escaping (_ error: OrdersOperationError?) -> Void)
  func updateOrder(order: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void)
  func deleteOrders(order: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void)
  func deleteSpecificOrderFromCoreData(orderId : String, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void)
}

enum OrdersStoreResult<U>
{
  case success(result: U)
  case failure(error: ProductsOperationError)
}

// MARK: - Orders store CRUD operation errorsa

enum OrdersOperationError: Equatable, Error
{
  case cannotFetch(String)
  case cannotCreate(String)
  case cannotUpdate(String)
  case cannotDelete(String)
}

func ==(lhs: OrdersOperationError, rhs: OrdersOperationError) -> Bool
{
  switch (lhs, rhs) {
  case (.cannotFetch(let a), .cannotFetch(let b)) where a == b: return true
  case (.cannotCreate(let a), .cannotCreate(let b)) where a == b: return true
  case (.cannotUpdate(let a), .cannotUpdate(let b)) where a == b: return true
  case (.cannotDelete(let a), .cannotDelete(let b)) where a == b: return true
  default: return false
  }
}
