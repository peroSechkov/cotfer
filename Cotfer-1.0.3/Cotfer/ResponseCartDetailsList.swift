//
//  ResponseCartDetailsList.swift
//  RVGPeem
//
//  Created by goran on 7/3/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct  ResponseCartDetailsList: Mappable {
  var CartHeaderInfo: ResponseCartHeader?
  var CartDetails: [ResponseCartDetails]?
  var ImagesInfo: ResponseImagesInfo?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CartHeaderInfo <- map["ResponseData.CartHeaderInfo"]
    CartDetails <- map["ResponseData.CartDetails"]
    ImagesInfo <- map["ResponseData.ImagesInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
