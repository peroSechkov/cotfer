//
//  ResponseContactList.swift
//  RVGContacts
//
//  Created by goran on 5/26/17.
//  Copyright © 2017 Darko Jovanovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseContactList: Mappable {
  
  var ContactListInfo: ResponseContactListInfo?
  var ContactList: [ResponseContact]?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ContactListInfo <- map["ResponseData.ContactListInfo"]
    ContactList <- map["ResponseData.ContactList"]
    ResponseInfo <- map["ResponseInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
  }
}
