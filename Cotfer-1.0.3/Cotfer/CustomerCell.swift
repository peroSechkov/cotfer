//
//  CustomerCell.swift
//  Cotfer
//
//  Created by goran on 7/31/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import UIKit

class CustomerCell: UICollectionViewCell {
  @IBOutlet weak var companyNameLabel: UILabel!
  @IBOutlet weak var firstNameLabel: UILabel!
  @IBOutlet weak var lastNameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var phoneLabel: UILabel!
}
