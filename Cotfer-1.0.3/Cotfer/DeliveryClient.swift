//
//  DeliveryClient.swift
//  Cotfer
//
//  Created by goran on 9/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class DeliveryClient: NSObject, Mappable {
  
  var ClientId: Int? 
  var Civility: String?
  var CompanyName: String?
  var CompanyName2: String?
  var Address: String?
  var Address2: String?
  var Zip: String?
  var City: String?
  var Country: String?
  var Region: String?
  var PhoneNumber: String?
  var PhoneNumber2: String?
  var FaxNumber: String?
  var MobileNumber: String?
  var MobileNumber2: String?
  var DeliveryInfo: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
    init(clientId: Int, address: String, country: String, phone: String, zip: String, city: String,companyName : String){
    self.Address = address
    self.Country = country
    self.PhoneNumber = phone
    self.Zip = zip
    self.City = city
    self.ClientId = clientId
    self.CompanyName = companyName
  }
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    ClientId <- map["ClientId"]
    Civility <- map["Civility"]
    CompanyName <- map["CompanyName"]
    CompanyName2 <- map["CompanyName2"]
    Address <- map["Address"]
    Address2 <- map["Address2"]
    Zip <- map["Zip"]
    City <- map["City"]
    Country <- map["Country"]
    Region <- map["Region"]
    PhoneNumber <- map["PhoneNumber"]
    PhoneNumber2 <- map["PhoneNumber2"]
    FaxNumber <- map["FaxNumber"]
    MobileNumber <- map["MobileNumber"]
    MobileNumber2 <- map["MobileNumber2"]
    DeliveryInfo <- map["DeliveryInfo"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}

extension DeliveryClient {
  convenience init(managedObject: ManagedDelivery) {
    self.init(clientId: Int(managedObject.clientId), address: managedObject.address!, country: managedObject.country!, phone: managedObject.mobileNumber!, zip: managedObject.zip!, city: managedObject.city!,companyName : managedObject.companyName!)
  }
}


