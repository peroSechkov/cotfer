//
//  ScreenSize.swift
//  RVGPeem
//
//  Created by goran on 7/25/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import UIKit

enum Screen: Int {
  case threePointFiveInch
  case fourInch
  case fourPointSevenInch
  case fivePointFiveInch
}

extension UIDevice {
  
  static var size: Screen {
    let bounds = UIScreen.main.bounds
    let maxLength = max(bounds.height, bounds.width)
    switch maxLength {
    case 480:
      return .threePointFiveInch
    case 568:
      return .fourInch
    case 667:
      return .fourPointSevenInch
    case 736:
      return .fivePointFiveInch
    default:
      return .fourPointSevenInch
    }
  }
  
}
