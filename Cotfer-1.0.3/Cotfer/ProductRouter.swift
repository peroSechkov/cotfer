//
//  ProductRouter.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum ProductRouter: URLRequestConvertible {
  case getProductByCategory(parameters: Parameters)
  case getProductCategoryNames(parameters: Parameters)
  case getAllProducts(parameters: Parameters)
  case getProductById(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .getProductCategoryNames, .getAllProducts, .getProductByCategory, .getProductById:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .getProductByCategory:
      return GET_CATEGORY_URL
    case .getProductCategoryNames:
      return CATEGORY_NAMES_URL
    case .getAllProducts:
      return ALL_PRODUCTS_URL
    case .getProductById:
      return PRODUCT_BY_ID_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try ProductRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .getAllProducts(let parameters), .getProductCategoryNames(let parameters), .getProductByCategory(let parameters), .getProductById(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }
}


