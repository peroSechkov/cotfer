//
//  ManagedProduct+CoreDataProperties.swift
//  Cotfer
//
//  Created by goran on 10/30/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//
//

import Foundation
import CoreData


extension ManagedProduct {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedProduct> {
        return NSFetchRequest<ManagedProduct>(entityName: "ManagedProduct")
    }

    @NSManaged public var barcode: String?
    @NSManaged public var capacity: Int32
    @NSManaged public var customerPrice: Double
    @NSManaged public var priceCurrency: String?
    @NSManaged public var productDescription: String?
    @NSManaged public var productId: String?
    @NSManaged public var productPrice: Double
    @NSManaged public var productPrice2: Double
    @NSManaged public var productPrice3: Double
    @NSManaged public var productPrice4: Double
    @NSManaged public var productPrice5: Double
    @NSManaged public var productPrice6: Double
    @NSManaged public var productPrice7: Double
    @NSManaged public var productPrice8: Double
    @NSManaged public var productPrice9: Double
    @NSManaged public var productPrice10: Double
    @NSManaged public var productTitle: String?
    @NSManaged public var unitCode: String?
    @NSManaged public var productQty1: Int64
    @NSManaged public var productQty2: Int64
    @NSManaged public var productQty3: Int64
    @NSManaged public var productQty4: Int64
    @NSManaged public var productQty5: Int64
    @NSManaged public var productQty6: Int64
    @NSManaged public var productQty7: Int64
    @NSManaged public var productQty8: Int64
    @NSManaged public var productQty9: Int64
    @NSManaged public var productQty10: Int64
    @NSManaged public var qtyStockAvailableAtTerm : Int64
    @NSManaged public var qtyStockAvailableImmediatly : Int64
    @NSManaged public var order: ManagedOrder?
    @NSManaged public var product: ManagedImage?
    @NSManaged public var category1 : String?
    @NSManaged public var category2 : String?

}
