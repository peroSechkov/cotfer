//
//  CompanyCell.swift
//  Cotfer
//
//  Created by goran on 9/1/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import UIKit

class CompanyCell: UITableViewCell {
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var textCoLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var memo1Button: UIButton!
    @IBOutlet weak var memo2Button: UIButton!
    @IBOutlet weak var memo3Button: UIButton!
    
    override func awakeFromNib() {
        memo1Button.setTitle(NSLocalizedString("memo1", comment: ""), for: .normal)
        memo2Button.setTitle(NSLocalizedString("memo2", comment: ""), for: .normal)
        memo3Button.setTitle(NSLocalizedString("memo3", comment: ""), for: .normal)
    }
}
