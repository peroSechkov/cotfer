//
//  UserWorker.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class UserWorker
{
  var userStore: UserStoreProtocol
  
  init(userStore: UserStoreProtocol) {
    self.userStore = userStore
  }
  
  func loginUser(parameters: Parameters) -> Observable<ResponseLogin> {
    return userStore.loginUser(parameters: parameters)
  }
  
  func employeeLogin(parameters: Parameters) -> Observable<ResponseEmployeeLogin> {
    return userStore.employeeLogin(parameters: parameters)
  }
}
protocol UserStoreProtocol {
  func loginUser(parameters: Parameters) -> Observable<ResponseLogin>
  func employeeLogin(parameters: Parameters) -> Observable<ResponseEmployeeLogin>
}


