//
//  CategoryNames.swift
//  Cotfer
//
//  Created by goran on 10/4/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct  CategoryNames: Mappable {
  
  var Cat1Name: String?
  var Cat2Name: String?
  var Cat3Name: String?
  var Cat4Name: String?
  var Cat5Name: String?
  var Cat6Name: String?
  var Cat7Name: String?
  var Cat8Name: String?
  var Cat9Name: String?
  var Cat10Name: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    Cat1Name <- map["Cat1Name"]
    Cat2Name <- map["Cat2Name"]
    Cat3Name <- map["Cat3Name"]
    Cat4Name <- map["Cat4Name"]
    Cat5Name <- map["Cat5Name"]
    Cat6Name <- map["Cat6Name"]
    Cat7Name <- map["Cat7Name"]
    Cat8Name <- map["Cat8Name"]
    Cat9Name <- map["Cat9Name"]
    Cat10Name <- map["Cat10Name"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
