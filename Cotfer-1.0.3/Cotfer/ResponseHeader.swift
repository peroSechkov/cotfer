//
//  ResponseHeader.swift
//  Cotfer
//
//  Created by goran on 10/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseHeader: Mappable {
  
  var CartHeaderInfo: ResponseCartHeader?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CartHeaderInfo <- map["ResponseData.CartHeaderInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
