//
//  ContactWorker.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import CoreStore

class ContactWorker
{
  var contactStore: ContactStoreProtocol
  
  init(contactStore: ContactStoreProtocol) {
    self.contactStore = contactStore
  }
  
  func addContact(parameters: Parameters) -> Observable<ResponseContact> {
    return contactStore.addContact(parameters: parameters)
  }
  
  func getContacts(parameters: Parameters) -> Observable<ResponseContactList> {
    return contactStore.getContacts(parameters: parameters)
  }
  
  func updateContact(parameters: Parameters) -> Observable<ResponseContact> {
    return contactStore.updateContact(parameters: parameters)
  }
  
  func searchContact(parameters: Parameters) -> Observable<ResponseContactList> {
    return contactStore.searchContact(parameters: parameters)
  }
  
  func contactRoles(parameters: Parameters) -> Observable<ResponseContact> {
    return contactStore.contactRoles(parameters: parameters)
  }
  
  func fetchStoredContacts(_ completionHandler: @escaping (_ contacts: [ManagedContact], _ error: ContactsOperationError?) -> Void){
    contactStore.fetchStoredContacts { (contacts: [ManagedContact], error) -> Void in
      completionHandler(contacts, error)
    }
  }
  
  func fetchContact(_ id: String, completionHandler: @escaping (_ contact: ResponseContact?, _ error: ContactsOperationError?) -> Void) {
    contactStore.fetchContact(id) {(contact, error) -> Void in
      completionHandler(contact, error)
    }
  }
  
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (_ error: ContactsOperationError?) -> Void) {
    contactStore.storeContact(contactToStore) { error in
      completionHandler(error)
    }
  }
  
  func updateContact(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void) {
    contactStore.updateContact(contact: contact, completionHandler: completionHandler)
  }
  
  func deleteContacts(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void) {
    
  }
  
}
protocol ContactStoreProtocol {
  func addContact(parameters: Parameters) -> Observable<ResponseContact>
  func getContacts(parameters: Parameters) -> Observable<ResponseContactList>
  func updateContact(parameters: Parameters) -> Observable<ResponseContact>
  func searchContact(parameters: Parameters) -> Observable<ResponseContactList>
  func contactRoles(parameters: Parameters) -> Observable<ResponseContact>
  func fetchStoredContacts(_ completionHandler: @escaping (_ contacts: [ManagedContact], _ error: ContactsOperationError?) -> Void)
  func fetchContact(_ id: String, completionHandler: @escaping (_ contact: ResponseContact?, _ error: ContactsOperationError?) -> Void)
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (_ error: ContactsOperationError?) -> Void)
  func updateContact(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void)
  func deleteContacts(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void)
}

enum ContactsStoreResult<U>
{
  case success(result: U)
  case failure(error: ContactsOperationError)
}

// MARK: - Orders store CRUD operation errorsa

enum ContactsOperationError: Equatable, Error
{
  case cannotFetch(String)
  case cannotCreate(String)
  case cannotUpdate(String)
  case cannotDelete(String)
}

func ==(lhs: ContactsOperationError, rhs: ContactsOperationError) -> Bool
{
  switch (lhs, rhs) {
  case (.cannotFetch(let a), .cannotFetch(let b)) where a == b: return true
  case (.cannotCreate(let a), .cannotCreate(let b)) where a == b: return true
  case (.cannotUpdate(let a), .cannotUpdate(let b)) where a == b: return true
  case (.cannotDelete(let a), .cannotDelete(let b)) where a == b: return true
  default: return false
  }
}

