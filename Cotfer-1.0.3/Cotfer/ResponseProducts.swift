//
//  ResponseProducts.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseProducts: Mappable {
  
  var ProductsListInfo: ProductsListInfo?
  var ProductsList: [ResponseProductItem]?
  var ImagesInfo: ResponseImagesInfo?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ProductsListInfo <- map["ResponseData.ProductListInfo"]
    ProductsList <- map["ResponseData.ProductList"]
    ImagesInfo <- map["ResponseData.ImagesInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
