//
//  CustomerService.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RxSwift
import CoreStore

class CustomerService: CustomerStoreProtocol {

  func fetchStoredCustomers(_ completionHandler: @escaping (_ customers: [ManagedCustomer], _ error: CustomersOperationError?) -> Void) {
    let customers = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedCustomer>()
    )
    let error: Error! = nil
    completionHandler(customers!, error as? CustomersOperationError)
  }
  
  func fetchStoredDeliveries(_ completionHandler: @escaping (_ deliveries: [ManagedDelivery], _ error: CustomersOperationError?) -> Void) {
    let deliveries = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedDelivery>()
    )
    let error: Error! = nil
    completionHandler(deliveries!, error as? CustomersOperationError)
  }
  
  func fetchCustomer(_ id: String, _ completionHandler: @escaping (_ customer: ResponseClient, _ error: CustomersOperationError?) -> Void) {
    if let customer = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedCustomer>()) {
    }
  }
  
  func fetchDelivery(_ id: String, _ completionHandler: @escaping (_ delivery: DeliveryClient, _ error: CustomersOperationError?) -> Void) {
    if let customer = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedDelivery>()) {
    }
  }
  
  func storeCustomer(_ customerToStore: ResponseClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let customer = transaction.create(Into<ManagedCustomer>())
        customer.address = customerToStore.Address
        customer.clientId = Int32(NSNumber(value: customerToStore.ClientId!))
        customer.companyName = customerToStore.CompanyName
        customer.email = customerToStore.Email
        customer.phone = customerToStore.Email
        
    },
      completion: { _ in
        let error: Error? = nil
        completionHandler(error as? CustomersOperationError)
    }
    )
    }
  
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let product = transaction.create(Into<ManagedProduct>())
        product.customerPrice = productToStore.ClientPrice!
        product.productId = productToStore.ProductCode!
        product.productPrice = productToStore.Price!
        product.productPrice2 = productToStore.Price2!
        product.productPrice3 = productToStore.Price3!
        product.productPrice4 = productToStore.Price4!
        product.productPrice5 = productToStore.Price5!
        product.productPrice6 = productToStore.Price6!
        product.productPrice7 = productToStore.Price7!
        product.productPrice8 = productToStore.Price8!
        product.productPrice9 = productToStore.Price9!
        product.productPrice10 = productToStore.Price10!
        product.productTitle = productToStore.ShortName!
        product.productDescription = productToStore.Description
        product.barcode = productToStore.BarCode!
        product.capacity = Int32(productToStore.Capacity!)
        product.unitCode = productToStore.UnitCode!
        product.priceCurrency = productToStore.PriceCurrency
        product.qtyStockAvailableAtTerm = Int64(productToStore.QtyStockAvailableAtTerm!)
        product.qtyStockAvailableImmediatly = Int64(productToStore.QtyStockAvailableImmediately!)
        product.category1 = productToStore.Cat1
        product.category2 = productToStore.Cat2
    },
      completion: { _ in
        let error: Error? = nil
        completionHandler(error as? CustomersOperationError)
      }
    )
  }
  
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let contact = transaction.create(Into<ManagedContact>())
        contact.address = contactToStore.Address
        contact.lastName = contactToStore.LastName
        contact.firstName = contactToStore.FirstName
        contact.email = contactToStore.Email
        contact.role = contactToStore.Position
        contact.phone = contactToStore.PhoneNumber
        contact.contactId = Int32(contactToStore.ContactId!)
    },
      completion: { _ in
        let error: Error? = nil
        completionHandler(error as? CustomersOperationError)
      }
    )
  }
  
  func storeDelivery(_ deliveryToStore: DeliveryClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let delivery = transaction.create(Into<ManagedDelivery>())
        delivery.address = deliveryToStore.Address
        delivery.clientId = Int32(deliveryToStore.ClientId!)
        delivery.companyName = deliveryToStore.CompanyName
        delivery.country = deliveryToStore.Country
        delivery.mobileNumber = deliveryToStore.PhoneNumber
        delivery.city = deliveryToStore.City
    },
      completion: { _ in }
    )
  }
  
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let image = transaction.create(Into<ManagedImage>())
        image.productId = imageToStore.productId
        image.picture = imageToStore.imageData! as NSData
    },
      completion: { _ in
        let error: Error? = nil
        completionHandler(error as? CustomersOperationError)
      }
    )
  }
  
  func updateCustomer(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        let managedCustomer = transaction.fetchOne( From(ManagedCustomer.self),
                                                    Where("%K == %@", "id", customer.ClientId!))
        
        managedCustomer?.address = customer.Address
        if let customerId = customer.ClientId {
          managedCustomer?.clientId = Int32(customerId)
        }
        managedCustomer?.companyName = customer.CompanyName
        managedCustomer?.country = customer.Country
        managedCustomer?.email = customer.Email
        managedCustomer?.phone = customer.PhoneNumber
        managedCustomer?.role = customer.EmployeeId
        
    },
      completion: { _ in }
    )
  }
  
  func updateDelivery(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        let managedDelivery = transaction.fetchOne( From(ManagedDelivery.self),
                                                    Where("%K == %@", "id", delivery.ClientId))
        
        managedDelivery?.address = delivery.Address
        managedDelivery?.address2 = delivery.Address2
        managedDelivery?.city = delivery.City
        if let clientId = delivery.ClientId {
          managedDelivery?.clientId = Int32(clientId)
        }
        managedDelivery?.companyName = delivery.CompanyName
        managedDelivery?.companyName2 = delivery.CompanyName2
        managedDelivery?.country = delivery.Country
        managedDelivery?.deliveryInfo = delivery.DeliveryInfo
        managedDelivery?.mobileNumber = delivery.MobileNumber
        managedDelivery?.phoneNumber = delivery.PhoneNumber
        managedDelivery?.zip = delivery.Zip
    },
      completion: { _ in }
    )

  }
  
  func deleteCustomers(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        transaction.deleteAll( From(ManagedCustomer.self) )
    },
      completion: { _ in }
    )
  }
  
  func deleteDeliveries(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        transaction.deleteAll( From(ManagedDelivery.self) )
    },
      completion: { _ in }
    )
  }
  
  func getClients(parameters: Parameters) -> Observable<ResponseClientList> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(CustomerRouter.getClients(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseClientList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func getContacts(parameters: Parameters) -> Observable<ResponseContactList> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ContactRouter.getContacts(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContactList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  } 
  
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ProductRouter.getAllProducts(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseProducts>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func getDeliveryInfo(parameters: Parameters) -> Observable<ResponseDeliveryList> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(CustomerRouter.getDeliveryList(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseDeliveryList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(OrderNetRouter.addOrder(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseCartDetailsList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
    
  }
  
  func customerRoles(parameters: Parameters) -> Observable<ResponseClient> {
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(CustomerRouter.customerRoles(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseClient>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
}
