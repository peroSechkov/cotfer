//
//  AppDelegate.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import UserNotifications
//import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    // Override point for customization after application launch.
    UIApplication.shared.isIdleTimerDisabled = true
    LanguageManager.setupCurrentLanguage()
    Fabric.with([Crashlytics.self])
    //IQKeyboardManager.sharedManager().enable = true
    let navigationBarAppearance = UINavigationBar.appearance()
    let barButtonItemAppearance = UIBarButtonItem.appearance()
    
    navigationBarAppearance.barTintColor = UIColor(red: 255/255.0, green: 38/255.0, blue: 16/255.0, alpha: 1)
    navigationBarAppearance.barStyle = .default
    navigationBarAppearance.tintColor = .white
    navigationBarAppearance.shadowImage = UIImage()
    navigationBarAppearance.isTranslucent = false
    navigationBarAppearance.backgroundColor = .clear
    navigationBarAppearance.titleTextAttributes = [
      NSFontAttributeName: UIFont.systemFont(ofSize: 17),
      NSForegroundColorAttributeName: UIColor.white
    ]
    
    barButtonItemAppearance.setTitleTextAttributes(
      [NSForegroundColorAttributeName: UIColor.white],
      for: .normal
    )
    barButtonItemAppearance.tintColor = .white
    
    let backButtonImage = UIImage(named: "backWhite")
    navigationBarAppearance.backIndicatorImage = backButtonImage
    navigationBarAppearance.backIndicatorTransitionMaskImage = backButtonImage
    
    if #available(iOS 10.0, *) {
        let center = UNUserNotificationCenter.current()
        center.delegate = self
        center.requestAuthorization(options: [.alert, .sound,.badge]) { (granted, error) in
            // actions based on whether notifications were authorized or not
            if granted {
                application.registerForRemoteNotifications()
            } else {
                
            }
        }
    } else {
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)
        
        application.registerUserNotificationSettings(pushNotificationSettings)
        application.registerForRemoteNotifications()
    }
    return true
  }

  func applicationWillResignActive(_ application: UIApplication) {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
  }

  func applicationDidEnterBackground(_ application: UIApplication) {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
  }

  func applicationWillEnterForeground(_ application: UIApplication) {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
  }

  func applicationDidBecomeActive(_ application: UIApplication) {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
  }

  func applicationWillTerminate(_ application: UIApplication) {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
  }
}

extension AppDelegate:UNUserNotificationCenterDelegate {
    
    //for displaying notification when app is in foreground
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        //If you don't want to show notification when app is open, do something here else and make a return here.
        //Even you you don't implement this delegate method, you will not see the notification on the specified controller. So, you have to implement this delegate and make sure the below line execute. i.e. completionHandler.
        
        completionHandler([.alert,.badge])
    }
}
