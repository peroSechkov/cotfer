//
//  SignatureRouter.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum SignatureRouter: URLRequestConvertible {
  case storeSignature(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .storeSignature:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .storeSignature:
      return STORE_SIGNATURE_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try SignatureRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .storeSignature(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }
}


