//
//  OrderService.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RxSwift
import CoreStore

class OrderService: OrderStoreProtocol {
    
    func fetchStoredOrders(_ completionHandler: @escaping (_ orders: [ManagedOrder], _ error: OrdersOperationError?) -> Void){
        let orders = DataStoreManager.sharedInstance.dataStack.fetchAll(
            From<ManagedOrder>()
        )
        let error: Error? = nil
        completionHandler(orders!, error as? OrdersOperationError)
    }
    
    func fetchOrder(_ id: String, completionHandler: @escaping (_ order: ResponseCartDetails?, _ error: OrdersOperationError?) -> Void){
        if let order = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedOrder>()) {
            
        }
    }
    
    func storeOrder(_ orderToStore: ResponseCartDetails, completionHandler: @escaping (_ error: OrdersOperationError?) -> Void){
        DataStoreManager.sharedInstance.dataStack.perform(
            asynchronous: { (transaction) in
                let order = transaction.create(Into<ManagedOrder>())
                order.orderId = orderToStore.ProductRef
                order.productDetail = orderToStore.Descr
                order.productName = orderToStore.Product
                order.productQuantity = orderToStore.Qty!
                order.totalPrice = orderToStore.Total!
                order.priceCurrency = orderToStore.Currency!
        },
            completion: { _ in
                let error: Error? = nil
                completionHandler(error as? OrdersOperationError)
        }
        )
    }
    
    func deleteSpecificOrderFromCoreData(orderId : String, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        DataStoreManager.sharedInstance.dataStack.perform(
            asynchronous: { (transaction) -> Void in
                let managedOrder = transaction.fetchOne( From(ManagedOrder.self),
                                                         Where("%K == %@", "orderId", orderId))
                transaction.delete(managedOrder)
        },
            completion: { _ in
                let error: Error? = nil
                completionHandler(error as? OrdersOperationError)
        })
    }
    
    func updateOrder(order: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        DataStoreManager.sharedInstance.dataStack.perform(
            asynchronous: { (transaction) -> Void in
                let managedOrder = transaction.fetchOne( From(ManagedOrder.self),
                                                         Where("%K == %@", "id", order.CartId!))
                
                managedOrder?.productName = order.Product
                //        if let orderID = order.CartId {
                //          managedOrder?.orderId = orderID
                //        }
                managedOrder?.productDetail = order.Descr
                if let orderQuantity = order.Qty {
                    managedOrder?.productQuantity = orderQuantity
                }
                if let total = order.Total {
                    managedOrder?.totalPrice = total
                }
        },
            completion: { _ in }
        )
        
    }
    
    func deleteOrders(order: ResponseCartDetails, completionHandler:  @escaping (_ error: OrdersOperationError?) -> Void) {
        DataStoreManager.sharedInstance.dataStack.perform(
            asynchronous: { (transaction) -> Void in
                transaction.deleteAll( From(ManagedOrder.self) )
        },
            completion: { _ in
                let error: Error? = nil
                completionHandler(error as? OrdersOperationError)
        }
        )
    }
    
    func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return Observable.create({ (observer) -> Disposable in
            let result = setManager().request(OrderNetRouter.addOrder(parameters: parameters))
                .responseObject { (response: DataResponse<ResponseCartDetailsList>) in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                    }
            }
            
            return Disposables.create{
                result.cancel()
            }
        })
        
    }
    
    func getOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return Observable.create({ (observer) -> Disposable in
            let result = setManager().request(OrderNetRouter.getOrder(parameters: parameters))
                .responseObject { (response: DataResponse<ResponseCartDetailsList>) in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        
                    }
            }
            
            return Disposables.create{
                result.cancel()
            }
        })
        
    }
    
    func updateOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return Observable.create({ (observer) -> Disposable in
            let result = setManager().request(OrderNetRouter.updateOrder(parameters: parameters))
                .responseObject { (response: DataResponse<ResponseCartDetailsList>) in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        
                    }
            }
            
            return Disposables.create{
                result.cancel()
            }
        })
        
    }
    
    func deleteOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
        return Observable.create({ (observer) -> Disposable in
            let result = setManager().request(OrderNetRouter.deleteOrder(parameters: parameters))
                .responseObject { (response: DataResponse<ResponseCartDetailsList>) in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        
                    }
            }
            
            return Disposables.create{
                result.cancel()
            }
        })
    }
    
    func checkoutOrder(parameters: Parameters) -> Observable<ResponseCheckout> {
        return Observable.create({ (observer) -> Disposable in
            let result = setManager().request(OrderNetRouter.checkoutOrder(parameters: parameters))
                .responseObject { (response: DataResponse<ResponseCheckout>) in
                    switch response.result {
                    case .success(let value):
                        observer.onNext(value)
                        observer.onCompleted()
                    case .failure(let error):
                        observer.onError(error)
                        
                    }
            }
            
            return Disposables.create{
                result.cancel()
            }
        })
    }
}



