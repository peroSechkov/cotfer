//
//  ResponseSignature.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct  ResponseSignature: Mappable {
  
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo : ResponseInfo?
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}

