//
//  CustomerRouter.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum CustomerRouter: URLRequestConvertible {
  case getClients(parameters: Parameters)
  case getDeliveryList(parameters: Parameters)
  case customerRoles(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .customerRoles, .getClients, .getDeliveryList:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .getClients:
      return GET_CLIENTS_URL
    case .customerRoles:
      return CUSTOMER_ROLES_URL
    case .getDeliveryList:
      return GET_DELIVERY_LIST_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try CustomerRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .getClients(let parameters), .customerRoles(let parameters), .getDeliveryList(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }
}

