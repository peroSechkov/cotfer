//
//  ResponseCategories.swift
//  Cotfer
//
//  Created by goran on 9/1/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseCategories: Mappable {
  
  var CategoryListInfo: ProductsListInfo?
  var CategoryList: [ResponseCategory]?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CategoryListInfo <- map["ResponseData.CategoryListInfo"]
    CategoryList <- map["ResponseData.CategoryList"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
