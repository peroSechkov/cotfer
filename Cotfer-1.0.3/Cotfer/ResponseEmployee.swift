//
//  ResponseEmployee.swift
//  Cotfer
//
//  Created by goran on 8/30/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseEmployee: Mappable {
  var EmployeeId: Int?
  var Code: String?
  var FirstName: String?
  var LastName: String?
  var Email: String?
  var Language: String?
  var AccessLevel: Int?
  var InactiveFlag: Int?
  
  init() {}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    EmployeeId <- map["ResponseData.EmployeeId"]
    Code <- map["ResponseData.Code"]
    FirstName <- map["ResponseData.FirstName"]
    LastName <- map["ResponseData.LastName"]
    Email <- map["ResponseData.Email"]
    Language <- map["ResponseData.Language"]
    AccessLevel <- map["ResponseData.AccessLevel"]
    InactiveFlag <- map["ResponseData.InactiveFlag"]
  }
}
