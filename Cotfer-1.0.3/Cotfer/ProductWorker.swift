//
//  ProductWorker.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import CoreStore

class ProductWorker
{
  var productStore: ProductStoreProtocol
  
  init(productStore: ProductStoreProtocol) {
    self.productStore = productStore
  }
  
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts> {
    return productStore.getAllProducts(parameters: parameters)
  }
  
  func getProductCategoryNames(parameters: Parameters) -> Observable<ResponseCategoryNames> {
    return productStore.getProductCategoryNames(parameters: parameters)
  }
  
  func getProductById(parameters: Parameters) -> Observable<ResponseProducts> {
    return productStore.getProductById(parameters: parameters)
  }
  
  func getProductsByCategory(parameters: Parameters) -> Observable<ResponseCategories> {
    return productStore.getProductsByCategory(parameters: parameters)
  }
  
  func getCartExist(parameters: Parameters) -> Observable<ResponseCartExist> {
    return productStore.getCartExist(parameters: parameters)
  }
  
  func fetchStoredProducts(_ completionHandler: @escaping (_ products: [ManagedProduct], _ error: ProductsOperationError?) -> Void){
    productStore.fetchStoredProducts { (products: [ManagedProduct], error) -> Void in
      completionHandler(products, error)
    }
  }
  
  func fetchProduct(_ id: String, completionHandler: @escaping (_ product: ResponseProductItem?, _ error: ProductsOperationError?) -> Void) {
    productStore.fetchProduct(id) {(product, error) -> Void in
      completionHandler(product, error)
    }
  }
  
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (_ error: ProductsOperationError?) -> Void) {
    productStore.storeProduct(productToStore) { error in
      completionHandler(error)
    }
  }
  
  func updateProduct(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    productStore.updateProduct(product: product, completionHandler: completionHandler)
  }
  
  func deleteProducts(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    
  }
  
  func fetchStoredImages(_ completionHandler: @escaping (_ images: [ManagedImage], _ error: ProductsOperationError?) -> Void) {
    productStore.fetchStoredImages { (images: [ManagedImage], error) -> Void in
      completionHandler(images, error)
    }
  }
  
   func fetchImage(_ id: String, completionHandler: @escaping (_ image: ManagedImage, _ error: ProductsOperationError?) -> Void){
    productStore.fetchImage(id) {(image, error) -> Void in
      completionHandler(image, error)
    }
  }
  
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: ProductsOperationError?) -> Void) {
    productStore.storeImage(imageToStore, completionHandler: { (error) in
      completionHandler(error)
    })
  }
  
  func updateImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
  
  }
  
  func deleteImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
  
  }
  
}

protocol ProductStoreProtocol {
  func getProductsByCategory(parameters: Parameters) -> Observable<ResponseCategories>
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts>
  func getProductCategoryNames(parameters: Parameters) -> Observable<ResponseCategoryNames>
  func getProductById(parameters: Parameters) -> Observable<ResponseProducts>
  func getCartExist(parameters: Parameters) -> Observable<ResponseCartExist>
  func fetchStoredProducts(_ completionHandler: @escaping (_ products: [ManagedProduct], _ error: ProductsOperationError?) -> Void)
  func fetchProduct(_ id: String, completionHandler: @escaping (_ product: ResponseProductItem?, _ error: ProductsOperationError?) -> Void)
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (_ error: ProductsOperationError?) -> Void)
  func updateProduct(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void)
  func deleteProducts(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void)
  func fetchStoredImages(_ completionHandler: @escaping (_ images: [ManagedImage], _ error: ProductsOperationError?) -> Void)
  func fetchImage(_ id: String, completionHandler: @escaping (_ image: ManagedImage, _ error: ProductsOperationError?) -> Void)
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: ProductsOperationError?) -> Void)
  func updateImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void)
  func deleteImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void)
}

enum ProductsStoreResult<U>
{
  case success(result: U)
  case failure(error: ProductsOperationError)
}

// MARK: - Orders store CRUD operation errorsa

enum ProductsOperationError: Equatable, Error
{
  case cannotFetch(String)
  case cannotCreate(String)
  case cannotUpdate(String)
  case cannotDelete(String)
}

func ==(lhs: ProductsOperationError, rhs: ProductsOperationError) -> Bool
{
  switch (lhs, rhs) {
  case (.cannotFetch(let a), .cannotFetch(let b)) where a == b: return true
  case (.cannotCreate(let a), .cannotCreate(let b)) where a == b: return true
  case (.cannotUpdate(let a), .cannotUpdate(let b)) where a == b: return true
  case (.cannotDelete(let a), .cannotDelete(let b)) where a == b: return true
  default: return false
  }
}

