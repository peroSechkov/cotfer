//
//  SignatureWorker.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift

class SignatureWorker
{
  var signatureStore: SignatureStoreProtocol
  
  init(signatureStore: SignatureStoreProtocol) {
    self.signatureStore = signatureStore
  }
  
  func storeSignature(parameters: Parameters) -> Observable<ResponseSignature> {
    return signatureStore.storeSignature(parameters: parameters)
  }
}
protocol SignatureStoreProtocol {
  func storeSignature(parameters: Parameters) -> Observable<ResponseSignature>
}

