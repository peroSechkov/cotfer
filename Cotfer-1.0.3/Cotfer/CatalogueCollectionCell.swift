//
//  CatalogueCollectionCell.swift
//  Cotfer
//
//  Created by goran on 7/28/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import UIKit

class CatalogueCollectionCell: UICollectionViewCell {
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var customerPriceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var barcodeImage: UIImageView!
    @IBOutlet weak var barcodeLabel: UILabel!
    @IBOutlet weak var articalNoLabel: UILabel!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var newProductImageView: UIImageView!
    @IBOutlet weak var liquidationImageView: UIImageView!
    @IBOutlet weak var promoImageView: UIImageView!
}
