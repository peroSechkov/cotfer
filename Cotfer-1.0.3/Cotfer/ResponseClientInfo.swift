//
//  ResponseClientInfo.swift
//  Cotfer
//
//  Created by goran on 9/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseClientInfo: Mappable {
  
  var ClientId: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ClientId <- map["ClientId"]
  }
}
