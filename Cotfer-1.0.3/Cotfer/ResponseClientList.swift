//
//  ResponseClientList.swift
//  RVGContacts
//
//  Created by goran on 5/25/17.
//  Copyright © 2017 Darko Jovanovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseClientList: Mappable {
  
    var ClientListInfo: ResponseClientListInfo?
    var ClientList: [ResponseClient]?
    var ResponseInfo: ResponseInfo?
    var ResponseUserInfo: ResponseUserInfo?
  
    init(){}
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
      ClientListInfo <- map["ResponseData.ClientListInfo"]
      ClientList <- map["ResponseData.ClientList"]
      ResponseInfo <- map["ResponseInfo"]
      ResponseUserInfo <- map["ResponseUserInfo"]
   }
}



