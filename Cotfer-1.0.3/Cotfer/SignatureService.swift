//
//  SignatureService.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RxSwift

class SignatureService: SignatureStoreProtocol {
  
  func storeSignature(parameters: Parameters) -> Observable<ResponseSignature> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(SignatureRouter.storeSignature(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseSignature>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
}


