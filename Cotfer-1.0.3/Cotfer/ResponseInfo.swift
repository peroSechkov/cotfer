//
//  ResponseInfo.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseInfo: Mappable {
  
  var Status: Int?
  var Message: Int?
  var ErrorCode: Int?
  var ErrorMessage: String?
  var ErrorDetails: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    Status <- map["Status"]
    Message <- map["Message"]
    ErrorCode <- map["ErrorCode"]
    ErrorMessage <- map["ErrorMessage"]
    ErrorDetails <- map["ErrorDetails"]
  }
}

