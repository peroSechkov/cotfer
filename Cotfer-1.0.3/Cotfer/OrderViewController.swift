//
//  OrderViewController.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import SwiftSpinner
import SwiftSignatureView

protocol OrderDisplayLogic: class
{
    func display(viewModel: OrderModel.ViewModel)
    func displayAddress(viewModel: OrderModel.ViewModel)
    func displayDelete(viewModel: OrderModel.ViewModel)
    func displayUpdate(viewModel: OrderModel.ViewModel)
    func displayCheckout(viewModel: OrderModel.ViewModel)
    func displayOrder(viewModel: OrderModel.ViewModel)
    func displayOrders(viewModel: OrderModel.ViewModel)
    func displayDeliveryInfo(viewModel: OrderModel.ViewModel)
    func displayDeleteFromCoreData()
}

class OrderViewController: UIViewController, OrderDisplayLogic, SwiftSignatureViewDelegate, BaseProtocol
{
    var interactor: OrderBusinessLogic?
    var router: (NSObjectProtocol & OrderRoutingLogic & OrderDataPassing)?
    var request = OrderModel.Request()
    var orders:[ResponseCartDetails] = []
    var managedOrders: [ManagedOrder]!
    @IBOutlet weak var orderTableView: UITableView!
    @IBOutlet weak var orderTotalPriceLabel: UILabel!
    @IBOutlet weak var acceptOrderButton: UIButton!
    @IBOutlet weak var paymentTermsPopUp: UIView!
    @IBOutlet weak var paymentTermsPopUpLabel: UILabel!
    @IBOutlet weak var paymentTermsDescriptionLabel: UITextView!
    @IBOutlet weak var paymentTermsCancelButton: UIButton!
    @IBOutlet weak var paymentTermsAgreeButton: UIButton!
    @IBOutlet weak var signatureView: SwiftSignatureView!
    @IBOutlet weak var signatureImage: UIImageView!
    @IBOutlet weak var deleveryView: UIView!
    @IBOutlet weak var clientView: UIView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var contactNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyDescLabel: UILabel!
    @IBOutlet weak var companyAddressLabel: UILabel!
    @IBOutlet weak var telephoneLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var postalCodeLabel: UILabel!
    @IBOutlet weak var fullNameLabel: UILabel!
    @IBOutlet weak var shippingLabel: UILabel!
    var sumTotal = 0.00
    var CrmInfo: CrmInfo!
    var UserInfo: ResponseUserInfo!
    var customer: ResponseClient!
    var contact: ResponseContact!
    var managedContact: ManagedContact!
    var Employee: Employee!
    var quantity: Double!
    @IBOutlet weak var writeSignatureLabel: UILabel!
    let userDefaults = UserDefaults.standard
    var isSideMenuClicked = false
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = OrderInteractor()
        let presenter = OrderPresenter()
        let router = OrderRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let employeeData = userDefaults.object(forKey: "employeeModel") {
            Employee = NSKeyedUnarchiver.unarchiveObject(with: employeeData as! Data) as! Employee
        }
        setupUI()
        
        orderTableView.tableFooterView = UIView()
        if isInternetAvailable() {
            if isSideMenuClicked {
                fetchOrders()
                fetchDeliveryInfo()
            } else {
                request.UserInfo = UserInfo
                request.CrmInfo = CrmInfo
                request.clientId = customer.ClientId
                request.contactId = contact.ContactId
                request.employeeId = Employee.EmployeeId
                companyNameLabel.text = customer.CompanyName
                companyDescLabel.text = customer.CompanyName2
                companyAddressLabel.text = customer.Address
                SwiftSpinner.show(NSLocalizedString("Loading", comment: ""))
                getOrder()
                getDeliveryInfo()
            }
        }else{
            fetchOrders()
            fetchDeliveryInfo()
        }
    }
    
    func setupUI(){
        deleveryView.layer.borderColor = UIColor.lightGray.cgColor
        deleveryView.layer.cornerRadius = 30
        deleveryView.layer.borderWidth = 3
        clientView.layer.borderColor = UIColor.lightGray.cgColor
        clientView.layer.borderWidth = 3
        clientView.layer.cornerRadius = 30
        acceptOrderButton.setTitle(NSLocalizedString("acceptorder", comment: ""), for: .normal)
        orderTotalPriceLabel.text = NSLocalizedString("totalorderprice", comment: "")
        paymentTermsPopUpLabel.text = NSLocalizedString("paymenttermslabel", comment: "")
        paymentTermsDescriptionLabel.text = NSLocalizedString("paymentermsdescription", comment: "")
        paymentTermsCancelButton.setTitle(NSLocalizedString("cancel", comment: ""), for: .normal)
        paymentTermsAgreeButton.setTitle(NSLocalizedString("agree", comment: ""), for: .normal)
        paymentTermsPopUp.alpha = 0.0
        paymentTermsPopUp.layer.cornerRadius = 10
        shippingLabel.text = NSLocalizedString("shipping", comment: "")
    }
    
    // MARK: Do something
    
    func getOrder() {
        interactor?.getOrder(request: request)
    }
    
    func getDeliveryInfo() {
        interactor?.getDeliveryList(request: request)
    }
    
    func chekoutOrder() {
        interactor?.checkoutOrder(request: request)
    }
    
    func updateOrder() {
        interactor?.updateOrder(request: request)
    }
    
    func deleteOrder() {
        interactor?.deleteOrder(request: request)
    }
    
    func deleteOrderFromCoreData(orderId : String) {
        interactor?.deleteOrderFromCoreData(orderId: orderId)
    }
    
    func fetchOrders() {
        interactor?.fetchOrders(request: request)
    }
    
    func fetchDeliveryInfo() {
        interactor?.fetchDeliveryInfo(request: request)
    }
    
    func display(viewModel: OrderModel.ViewModel) {
        SwiftSpinner.hide()
        if viewModel.viewModel.CartDetails != nil {
            sumTotal = (viewModel.viewModel.CartHeaderInfo?.TotalCart!)!
            orderTotalPriceLabel.text = NSLocalizedString("Total price:", comment: "") + "  " + "\(String(format: "%.2f", sumTotal))" + (viewModel.viewModel.CartHeaderInfo?.Currency!)!
            orders = viewModel.viewModel.CartDetails!
        }else{
            orderTotalPriceLabel.text = NSLocalizedString("Total price:", comment: "") + "  " + "\(String(format: "%.2f", 0.00))" + NSLocalizedString("CHF", comment: "")
        }
        if orders.count == 0 {
            acceptOrderButton.isEnabled = false
        }
        orderTableView.reloadData()
    }
    
    func displayDelete(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelDelete != nil {
            getOrder()
        }
        orderTableView.reloadData()
    }
    
    func  displayDeleteFromCoreData() {
        fetchOrders()
        orderTableView.reloadData()
    }
    
    func displayUpdate(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelUpdate != nil {
            orders = viewModel.viewModelUpdate.CartDetails!
        }
        orderTableView.reloadData()
    }
    
    func displayCheckout(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelCheckoutOrder != nil {
            weak var weakSelf = self
            //image for testing purposes
            let alert = UIAlertController(title: NSLocalizedString("Purchase Confirmed", comment: ""), message: NSLocalizedString("Your purchase was processed", comment: ""), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: UIAlertActionStyle.default, handler: {action in
                weakSelf?.navigationController?.popViewController(animated: false)
                //weakSelf?.userDefaults.removeObject(forKey: "userInfoModel")
            })
            )
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func displayAddress(viewModel: OrderModel.ViewModel) {
        SwiftSpinner.hide()
        if viewModel.viewModelAddress != nil {
            let address = viewModel.viewModelAddress.DeliveryClientList?[0]
            if !isSideMenuClicked {
                contactNameLabel.text = contact.FirstName! + " " + contact.LastName!
                fullNameLabel.text = contact.FirstName! + " " + contact.LastName!
            } else {
                contactNameLabel.text = managedContact.firstName! + " " + managedContact.lastName!
                fullNameLabel.text = managedContact.firstName! + " " + managedContact.lastName!
            }
            addressLabel.text = address?.Address
            postalCodeLabel.text = address?.Zip
            cityLabel.text = address?.City
            countryLabel.text = address?.Country
            telephoneLabel.text = address?.MobileNumber
        }
    }
    
    func displayOrders(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelOrders != nil {
            managedOrders = viewModel.viewModelOrders
            if managedOrders.count > 0 {
                sumTotal = 0
                for managedOrder in managedOrders {
                    sumTotal += managedOrder.totalPrice
                }
                orderTotalPriceLabel.text = NSLocalizedString("Total price:", comment: "") + "  " + "\(String(format: "%.2f", sumTotal))" + " CHF"
                orderTableView.reloadData()
            } else {
                orderTotalPriceLabel.text = NSLocalizedString("Total price:", comment: "") + "  " + "\(String(format: "%.2f", 0.00))" + " CHF"
            }
        }
    }
    
    func displayDeliveryInfo(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelDeliveryInfo != nil {
            let managedDelivery = viewModel.viewModelDeliveryInfo
            contactNameLabel.text = managedContact.firstName! + " " + managedContact.lastName!
            addressLabel.text = managedDelivery?.address
            postalCodeLabel.text = managedDelivery?.zip
            cityLabel.text = managedDelivery?.city
            countryLabel.text = managedDelivery?.country
            telephoneLabel.text = managedDelivery?.mobileNumber
            companyNameLabel.text = managedDelivery?.companyName
            companyAddressLabel.text = managedDelivery?.address
        }
    }
    
    @IBAction func acceptOrderButtonTapped(_ sender: Any) {
        weak var weakSelf = self
        UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseOut,
                       animations: {      weakSelf?.paymentTermsPopUp.alpha = 1},
                       completion: { _ in weakSelf?.paymentTermsPopUp.isHidden = false
        })
    }
    
    @IBAction func cancelButtonTapped(_ sender: Any) {
        weak var weakSelf = self
        UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseIn,
                       animations: {  weakSelf?.paymentTermsPopUp.alpha = 0},
                       completion: { _ in weakSelf?.paymentTermsPopUp.isHidden = true
                        weakSelf?.signatureView.clear()
        })
        
    }
    
    @IBAction func acceptButtonTapped(_ sender: Any) {
        weak var weakSelf = self
        UIView.animate(withDuration: 0.2, delay: 0.2, options: .curveEaseIn,
                       animations: {  weakSelf?.paymentTermsPopUp.alpha = 0},
                       completion: { _ in weakSelf?.paymentTermsPopUp.isHidden = true
                        let imageData : Data = UIImagePNGRepresentation((weakSelf?.signatureView.signature)!)! as Data
                        weakSelf?.request.signatureContent = imageData.base64EncodedString(options: Data.Base64EncodingOptions.init(rawValue: 0))
                        weakSelf?.request.imageFileName = signatureName + "\(String(describing: weakSelf?.contact.ContactId))" + imageExtension
                        weakSelf?.signatureView.clear()
                        weakSelf?.chekoutOrder()
        })
    }
    
    @IBAction func termsAndConditionsTapped(_ sender: Any) {
        performSegue(withIdentifier: "showTermsAndConditionsVC", sender: self)
    }
    
    
    public func swiftSignatureViewDidTapInside(_ view: SwiftSignatureView) {
        print("Did tap inside")
    }
    
    public func swiftSignatureViewDidPanInside(_ view: SwiftSignatureView) {
        print("Did pan inside")
    }
    
    func createOrder(order: ResponseCartDetails) {
        request.orderId = order.ProductRef
        request.productDetail = order.description
        request.productName = order.Product
        request.productQuantity = order.Qty
        request.LineId = order.LineId
        request.totalPrice = order.Total
        interactor?.createOrder(request: request)
    }
    
    func displayOrder(viewModel: OrderModel.ViewModel) {
        if viewModel.viewModelOrder != nil {
            
        }
    }
    
}

extension OrderViewController: UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isInternetAvailable(){
            if isSideMenuClicked {
               return managedOrders != nil ? managedOrders.count : 0
            } else {
               return orders != nil ? orders.count : 0
            }
        }else{
            return managedOrders != nil ? managedOrders.count : 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderCell", for: indexPath) as! OrderCell
        if isInternetAvailable() == true {
            if isSideMenuClicked {
                let order = managedOrders[indexPath.row]
                cell.productNameLabel.text = order.productName
                cell.productDetialLabel.text = order.productDetail
                let quantity = Int(order.productQuantity)
                cell.productQtyTextField.text = "\(quantity)"
                cell.productTotalPriceLabel.text =  String(format: "%.2f",order.totalPrice) + " CHF"
            } else {
                let order = orders[indexPath.row]
                cell.productNameLabel.text = order.Product
                cell.productDetialLabel.text = order.Descr
                let quantity = Int(order.Qty!)
                cell.productQtyTextField.text = "\(quantity)"
                cell.productTotalPriceLabel.text =  String(format: "%.2f", order.Total!) + " " + order.Currency!
                
            }
        } else {
            let order = managedOrders[indexPath.row]
            cell.productNameLabel.text = order.productName
            cell.productDetialLabel.text = order.productDetail
            let quantity = Int(order.productQuantity)
            cell.productQtyTextField.text = "\(quantity)"
            cell.productTotalPriceLabel.text =  String(format: "%.2f",order.totalPrice) + " CHF"
        }
        cell.productQtyTextField.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if isInternetAvailable() {
            if isSideMenuClicked {
                let order = managedOrders[indexPath.row]
                let orderId = order.orderId!
                deleteOrderFromCoreData(orderId : orderId)
            } else {
                let order = orders[indexPath.row]
                if editingStyle == .delete {
                    orders.remove(at: indexPath.row)
                    request.LineId = order.LineId
                    deleteOrder()
                }
                
            }
        } else {
            let order = managedOrders[indexPath.row]
            let orderId = order.orderId!
            deleteOrderFromCoreData(orderId : orderId)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let order: ResponseCartDetails!
        let managedOrder: ManagedOrder!
        let tag = textField.tag
        
        if isInternetAvailable() {
            if isSideMenuClicked {
                order = orders[tag]
                request.ProductRef = order.Product
                request.LineId = order.LineId
                quantity = Double(textField.text!)!
                request.Quantity = quantity
                updateOrder()
            } else {
                managedOrder = managedOrders[tag]
                request.ProductRef = managedOrder.productName
                request.LineId = Int(managedOrder.lineNumber)
                quantity = Double(textField.text!)!
            }
        }else{
            managedOrder = managedOrders[tag]
            request.ProductRef = managedOrder.productName
            request.LineId = Int(managedOrder.lineNumber)
            quantity = Double(textField.text!)!
        }
        textField.resignFirstResponder()
        return true
    }
}
