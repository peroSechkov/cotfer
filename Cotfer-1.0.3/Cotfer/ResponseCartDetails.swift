//
//  ResponseCartDetails.swift
//  RVGPeem
//
//  Created by goran on 6/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseCartDetails:NSObject, Mappable {
  
  var ProductRef: String?
  var Product: String?
  var LineId: Int?
  var LineNum: Int?
  var Descr: String?
  var TaxInc: Double?
  var CartId: Int?
  var UnitDescr: String?
  var Qty: Double?
  var UnitCode: String?
  var UnitQty: Double?
  var ItemNumber: Int?
  var PricePer: Int?
  var DiscountPro: Int?
  var DiscountQty: Int?
  var DiscountTotal: Int?
  var TotalOptions: Int?
  var UnitPrimaryPrice: Double?
  var UnitNetPrice: Double?
  var DiscountPercent: Int?
  var Total: Double?
  var Currency: String?
  var ImageName: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  init(productRef:String, productDetail:String, productName:String, productQuantity:Double, totalPrice: Double, priceCurrency: String, lineId: Int){
    self.ProductRef = productRef
    self.Descr = productDetail
    self.Product = productName
    self.Qty = productQuantity
    self.Total = totalPrice
    self.Currency = priceCurrency
    self.LineId = lineId
  }
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    Product <- map["Product"]
    LineId <- map["LineId"]
    LineNum <- map["LineNum"]
    Descr <- map["Descr"]
    TaxInc <- map["TaxInc"]
    CartId <- map["CartId"]
    UnitDescr <- map["UnitDescr"]
    Qty <- map["Qty"]
    UnitCode <- map["UnitCode"]
    UnitQty <- map["UnitQty"]
    ItemNumber <- map["ItemNumber"]
    PricePer <- map["PricePer"]
    DiscountPro <- map["DiscountPro"]
    DiscountQty <- map["DiscountQty"]
    DiscountTotal <- map["DiscountTotal"]
    TotalOptions <- map["TotalOptions"]
    UnitPrimaryPrice <- map["UnitPrimaryPrice"]
    UnitNetPrice <- map["UnitNetPrice"]
    DiscountPercent <- map["DiscountPercent"]
    Total <- map["Total"]
    Currency <- map["Currency"]
    ImageName <- map["ImageName"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}

extension ResponseCartDetails {
  convenience init(managedObject: ManagedOrder) {
    self.init(productRef: managedObject.orderId!, productDetail:managedObject.productDetail!, productName:managedObject.productName!, productQuantity:Double(managedObject.productQuantity), totalPrice: managedObject.totalPrice, priceCurrency: managedObject.priceCurrency!, lineId: Int(managedObject.lineNumber))
  }
}
