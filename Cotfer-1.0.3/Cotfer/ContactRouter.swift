//
//  ContactRouter.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum ContactRouter: URLRequestConvertible {
  case addContact(parameters: Parameters)
  case updateContact(parameters: Parameters)
  case getContacts(parameters: Parameters)
  case searchContact(parameters: Parameters)
  case contactRoles(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .addContact, .getContacts, .updateContact, .searchContact, .contactRoles:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .addContact:
      return ADD_CONTACT_URL
    case .updateContact:
      return UPDATE_CONTACT_URL
    case .getContacts:
      return GET_CONTACTS_URL
    case .searchContact:
      return SEARCH_CONTACT_URL
    case .contactRoles:
      return CONTACT_ROLES_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try ContactRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .addContact(let parameters), .updateContact(let parameters), .getContacts(let parameters), .searchContact(let parameters), .contactRoles(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }
}

