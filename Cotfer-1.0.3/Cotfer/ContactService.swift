//
//  ContactService.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper
import RxSwift
import CoreStore

class ContactService: ContactStoreProtocol {

  
  func fetchStoredContacts(_ completionHandler: @escaping ([ManagedContact], ContactsOperationError?) -> Void) {
    let contacts = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedContact>()
    )
    let error: Error! = nil
    completionHandler(contacts!, error as? ContactsOperationError)
  }
  
  func fetchContact(_ id: String, completionHandler: @escaping (ResponseContact?, ContactsOperationError?) -> Void) {
    if let contact = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedContact>()) {
    }
    let error: Error! = nil
 //   completionHandler(contact!, error as? ContactsOperationError)
  }
  
  func getContactsByClientId(_ id: String, _ completionHandler: @escaping (_ contacts: [ManagedContact], _ error: ContactsOperationError?) -> Void) {
    
    let contacts = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedContact>()
    )
    let error: Error? = nil
    completionHandler(contacts!, error as? ContactsOperationError)
    
  }
  
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (ContactsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let contact = transaction.create(Into<ManagedContact>())
        contact.address = contactToStore.Address
        contact.lastName = contactToStore.LastName
        contact.firstName = contactToStore.FirstName
        contact.email = contactToStore.Email
        contact.role = contactToStore.Position
        contact.phone = contactToStore.PhoneNumber
    },
      completion: { _ in }
    )
  }
  
  func updateContact(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        let managedContact = transaction.fetchOne( From(ManagedContact.self),
                                                   Where("%K == %@", "id", contact.ContactId!))
        if let contactId = contact.ContactId {
          managedContact?.contactId = Int32(contactId)
        }
        managedContact?.email = contact.Email
        managedContact?.firstName = contact.FirstName
        managedContact?.lastName = contact.LastName
        managedContact?.phone = contact.PhoneNumber
        managedContact?.address = contact.Address
        managedContact?.role = contact.Position
    },
      completion: { _ in }
    )
  }
  
  func deleteContacts(contact: ResponseContact, completionHandler:  @escaping (_ error: ContactsOperationError?) -> Void) {
    
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        transaction.deleteAll( From(ManagedContact.self) )
    },
      completion: { _ in }
    )
  }
  
  func searchContact(parameters: Parameters) -> Observable<ResponseContactList> {
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(ContactRouter.searchContact(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContactList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }

  
  func addContact(parameters: Parameters) -> Observable<ResponseContact> {
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(ContactRouter.addContact(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContact>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }

  func getContacts(parameters: Parameters) -> Observable<ResponseContactList> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ContactRouter.getContacts(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContactList>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func updateContact(parameters: Parameters) -> Observable<ResponseContact> {
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(ContactRouter.updateContact(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContact>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func contactRoles(parameters: Parameters) -> Observable<ResponseContact> {
    return Observable.create({ (observer) -> Disposable in
      let result = Alamofire.request(ContactRouter.contactRoles(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseContact>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
}

