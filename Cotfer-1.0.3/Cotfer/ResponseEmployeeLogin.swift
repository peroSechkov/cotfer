//
//  ResponseEmployeeLogin.swift
//  Cotfer
//
//  Created by goran on 9/19/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseEmployeeLogin: Mappable {
  
  var AccessToken: String?
  var CrmInfo: CrmInfo?
  var Employee: Employee?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    AccessToken <- map["ResponseData.AccessToken"]
    CrmInfo <- map["ResponseData.CrmInfo"]
    Employee <- map["ResponseData.Employee"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
