//
//  OrderCell.swift
//  Cotfer
//
//  Created by goran on 7/28/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import UIKit

class OrderCell: UITableViewCell {
  @IBOutlet weak var productNameLabel: UILabel!
  @IBOutlet weak var productDetialLabel: UILabel!
  @IBOutlet weak var productQtyTextField: UITextField!
  @IBOutlet weak var productTotalPriceLabel: UILabel!
}
