//
//  ManagedDelivery+CoreDataProperties.swift
//  Cotfer
//
//  Created by Jovan Horvat on 9/22/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import CoreData


extension ManagedDelivery {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedDelivery> {
        return NSFetchRequest<ManagedDelivery>(entityName: "ManagedDelivery")
    }

    @NSManaged public var clientId: Int32
    @NSManaged public var companyName: String?
    @NSManaged public var companyName2: String?
    @NSManaged public var zip: String?
    @NSManaged public var address2: String?
    @NSManaged public var address: String?
    @NSManaged public var city: String?
    @NSManaged public var country: String?
    @NSManaged public var phoneNumber: String?
    @NSManaged public var mobileNumber: String?
    @NSManaged public var deliveryInfo: String?

}
