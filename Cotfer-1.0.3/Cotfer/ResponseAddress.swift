//
//  ResponseAddress.swift
//  RVGPeem
//
//  Created by goran on 6/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseAddress: Mappable {
  
  var AddressNo: Int?
  var Honorific: String?
  var FirstName: String?
  var LastName: String?
  var CompanyName: String?
  var Address: String?
  var Address2: String?
  var PostalCode: String?
  var City: String?
  var Country: String?
  var TelNo: String?
  var TelNo2: String?
  var FaxNo: String?
  var MobileNo: String?
  var MobileNo2: String?
  var Email: String?
  var IsBillingAddress: Int?
  var FirstLastName: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    AddressNo <- map["AddressNo"]
    Honorific <- map["Honorific"]
    FirstName <- map["FirstName"]
    LastName <- map["LastName"]
    CompanyName <- map["CompanyName"]
    Address <- map["Address"]
    Address2 <- map["Address2"]
    PostalCode <- map["PostalCode"]
    City <- map["City"]
    Country <- map["Country"]
    TelNo <- map["TelNo"]
    TelNo2 <- map["TelNo2"]
    FaxNo <- map["FaxNo"]
    MobileNo <- map["MobileNo"]
    MobileNo2 <- map["MobileNo2"]
    Email <- map["Email"]
    IsBillingAddress <- map["IsBillingAddress"]
    FirstLastName <- map["FirstLastName"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
