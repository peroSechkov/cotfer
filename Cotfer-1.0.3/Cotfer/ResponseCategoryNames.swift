//
//  ResponseCategoryNames.swift
//  Cotfer
//
//  Created by goran on 10/4/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseCategoryNames: Mappable {
  
  var CategoryNames: CategoryNames?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CategoryNames <- map["ResponseData.CategoryNames"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
