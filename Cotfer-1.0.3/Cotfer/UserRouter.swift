//
//  UserRouter.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum UserRouter: URLRequestConvertible {
  case userLogin(parameters: Parameters)
  case employeeLogin(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .userLogin, .employeeLogin:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .userLogin:
      return USER_LOGIN_URL
    case .employeeLogin:
      return EMPLOYEE_LOGIN_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try UserRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .userLogin(let parameters), .employeeLogin(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }

}

