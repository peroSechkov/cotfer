//
//  ResponseShopItem.swift
//  RVGPeem
//
//  Created by goran on 7/3/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseShopItem: NSObject, Mappable, NSCoding {
  var Server: String?
  var Db: String?
  var DbUser: String?
  var DbPass: String?
  var ClientName: String?
  var ClientLogo: String?
  var ClientWebsite: String?
  var AllowAccessProducts: Bool?
  var AllowAccessWishlist: Bool?
  var AllowAccess: Bool?
  var Status: Int?
  var NameViewProducts: String?
  var NameViewWishlist: String?
  var ProductPopupType: Int?
  var ImagePathMin: String?
  var ImagePathMax: String?
  var ImageExtension: String?
  var MerchantId_For_iOS: String?
  var MerchantName_For_iOS: String?
  var MerchantSignature_For_iOS: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  override init(){}
  
  required init?(map: Map) {}
  
  init(shopInfo: ResponseShopItem) {
    self.Server = shopInfo.Server
    self.Db = shopInfo.Db
    self.DbUser = shopInfo.DbUser
    self.DbPass = shopInfo.DbPass
    self.ClientName = shopInfo.ClientName
    self.ClientLogo = shopInfo.ClientLogo
    self.ClientWebsite = shopInfo.ClientWebsite
    self.ImagePathMax = shopInfo.ImagePathMax
    self.ImagePathMin = shopInfo.ImagePathMin
    self.ImageExtension = shopInfo.ImageExtension
    self.MerchantId_For_iOS = shopInfo.MerchantId_For_iOS
    self.MerchantName_For_iOS = shopInfo.MerchantName_For_iOS
    self.MerchantSignature_For_iOS = shopInfo.MerchantSignature_For_iOS
    self.ShopNumber = shopInfo.ShopNumber
  }
  
  required init(coder decoder: NSCoder) {
    self.Server = decoder.decodeObject(forKey: "Server") as? String ?? ""
    self.Db = decoder.decodeObject(forKey: "Db") as? String ?? ""
    self.DbUser = decoder.decodeObject(forKey: "DbUser") as? String ?? ""
    self.DbPass =  decoder.decodeObject(forKey: "DbPass") as? String ?? ""
    self.ClientName =  decoder.decodeObject(forKey: "ClientName") as? String ?? ""
    self.ClientLogo =  decoder.decodeObject(forKey: "ClientLogo") as? String ?? ""
    self.ClientWebsite = decoder.decodeObject(forKey: "ClientWebsite") as? String ?? ""
    self.ImagePathMax =  decoder.decodeObject(forKey: "ImagePathMax") as? String ?? ""
    self.ImagePathMin =  decoder.decodeObject(forKey: "ImagePathMin") as? String ?? ""
    self.ImageExtension =  decoder.decodeObject(forKey: "ImageExtension") as? String ?? ""
    self.MerchantId_For_iOS = decoder.decodeObject(forKey: "MerchantId_For_iOS") as? String ?? ""
    self.MerchantName_For_iOS = decoder.decodeObject(forKey: "MerchantName_For_iOS") as? String ?? ""
    self.MerchantSignature_For_iOS = decoder.decodeObject(forKey: "MerchantSignature_For_iOS") as? String ?? ""
    self.ShopNumber =  decoder.decodeObject(forKey: "ShopNumber") as? Int ?? 0
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(Server, forKey: "Server")
    aCoder.encode(Db, forKey: "Db")
    aCoder.encode(DbUser, forKey: "DbUser")
    aCoder.encode(DbPass, forKey: "DbPass")
    aCoder.encode(ClientName, forKey: "ClientName")
    aCoder.encode(ClientLogo, forKey: "ClientLogo")
    aCoder.encode(ClientWebsite, forKey: "ClientWebsite")
    aCoder.encode(ImagePathMax, forKey: "ImagePathMax")
    aCoder.encode(ImagePathMin, forKey: "ImagePathMin")
    aCoder.encode(ImageExtension, forKey: "ImageExtension")
    aCoder.encode(MerchantId_For_iOS, forKey: "MerchantId_For_iOS")
    aCoder.encode(MerchantName_For_iOS, forKey: "MerchantName_For_iOS")
    aCoder.encode(MerchantSignature_For_iOS, forKey: "MerchantSignature_For_iOS")
    aCoder.encode(ShopNumber, forKey: "ShopNumber")
  }
  
  func mapping(map: Map) {
    Status <- map["Status"]
    Server <- map["Server"]
    Db <- map["Db"]
    DbUser <- map["DbUser"]
    DbPass <- map["DbPass"]
    ClientName <- map["ClientName"]
    ClientLogo <- map["ClientLogo"]
    ClientWebsite <- map["ClientWebsite"]
    AllowAccessProducts <- map["AllowAccessProducts"]
    AllowAccessWishlist <- map["AllowAccessWishlist"]
    AllowAccess <- map["AllowAccess"]
    NameViewProducts <- map["NameViewProducts"]
    NameViewWishlist <- map["NameViewWishlist"]
    ProductPopupType <- map["ProductPopupType"]
    ImagePathMin <- map["ImagePathMin"]
    ImagePathMax <- map["ImagePathMax"]
    ImageExtension <- map["ImageExtension"]
    MerchantId_For_iOS <- map["MerchantId_For_iOS"]
    MerchantName_For_iOS <- map["MerchantName_For_iOS"]
    MerchantSignature_For_iOS <- map["MerchantSignature_For_iOS"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
