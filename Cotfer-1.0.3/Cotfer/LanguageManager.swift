//
//  LanguageManager.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
private let LanguageCodes: [String] = ["fr", "en", "de"]
private let LanguageStrings: [String] = ["French", "English", "German"]
private let LanguageSaveKey: String = "currentLanguageKey"

enum ELanguage : Int {
  case english
  case german
  case french
}

class LanguageManager {
  class func setupCurrentLanguage() {
    var currentLanguage: String? = UserDefaults.standard.object(forKey: LanguageSaveKey) as? String
    if currentLanguage == nil {
      let languages: [String]? = UserDefaults.standard.object(forKey: "AppleLanguages") as? [String]
      if (languages?.count)! > 0 {
        currentLanguage = languages?[0]
        UserDefaults.standard.set(currentLanguage, forKey: LanguageSaveKey)
        UserDefaults.standard.synchronize()
      }
    }
    #if !USE_ON_FLY_LOCALIZATION
      UserDefaults.standard.set([currentLanguage], forKey: "AppleLanguages")
      UserDefaults.standard.synchronize()
    #else
      Bundle.language = currentLanguage
    #endif
  }
  
  class func languageStrings() -> [Any] {
    var array = [Any]()
    for i in 0..<LanguageCodes.count {
      array.append(NSLocalizedString(LanguageStrings[i], comment: ""))
    }
    return array
  }
  
  class func currentLanguageString() -> String {
    var string: String = ""
    let currentCode: String? = UserDefaults.standard.object(forKey: LanguageSaveKey) as? String
    for i in 0..<LanguageCodes.count {
      if (currentCode == LanguageCodes[i]) {
        string = NSLocalizedString(LanguageStrings[i], comment: "")
        break
      }
    }
    return string
  }
  
  class func currentLanguageCode() -> String {
    return UserDefaults.standard.object(forKey: LanguageSaveKey)! as! String
  }
  
  class func currentLanguageIndex() -> Int {
    var index: Int = 0
    let currentCode: String? = UserDefaults.standard.object(forKey: LanguageSaveKey) as? String
    for i in 0..<LanguageCodes.count {
      if (currentCode == LanguageCodes[i]) {
        index = i
        break
      }
    }
    return index
  }
  
  class func saveLanguage(by index: Int) {
    if index >= 0 && index < LanguageCodes.count {
      let code: String = LanguageCodes[index]
      UserDefaults.standard.set(code, forKey: LanguageSaveKey)
      UserDefaults.standard.synchronize()
      #if USE_ON_FLY_LOCALIZATION
        Bundle.language = code
      #endif
    }
  }
  
  class func isCurrentLanguageRTL() -> Bool {
    let currentLanguageIndex: Int = self.currentLanguageIndex()
    return (NSLocale.characterDirection(forLanguage: LanguageCodes[currentLanguageIndex]) == .rightToLeft)
  }
  
}

#if USE_ON_FLY_LOCALIZATION
  private let kBundleKey = 0
  
  class BundleEx: NSBundle {
    override func localizedString(forKey key: String, value: String, table tableName: String) -> String {
      let bundle: Bundle? = objc_getAssociatedObject(self, kBundleKey)
      if bundle != nil {
        return bundle?.localizedString(forKey: key, value: value, table: tableName)!
      }
      else {
        return super.localizedString(forKey: key, value: value, table: tableName)
      }
    }
  }
  
  extension NSBundle {
    class func setLanguage(_ language: String) {
      var onceToken: Int
      if (onceToken == 0) {
        object_setClass(Bundle.main, BundleEx.self)
      }
      onceToken = 1
      if LanguageManager.isCurrentLanguageRTL() {
        if UIView().responds(to: #selector(self.setSemanticContentAttribute)) {
          UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }
      }
      else {
        if UIView().responds(to: #selector(self.setSemanticContentAttribute)) {
          UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
      }
      UserDefaults.standard.set(LanguageManager.isCurrentLanguageRTL(), forKey: "AppleTextDirection")
      UserDefaults.standard.set(LanguageManager.isCurrentLanguageRTL(), forKey: "NSForceRightToLeftWritingDirection")
      UserDefaults.standard.synchronize()
      let value: Any? = language ? Bundle(path: Bundle.main.path(forResource: language, ofType: "lproj")) : nil
      objc_setAssociatedObject(Bundle.main, kBundleKey, value, OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
  }
#endif
