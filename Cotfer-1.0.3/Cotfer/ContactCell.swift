//
//  ContactCell.swift
//  Cotfer
//
//  Created by goran on 9/3/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import UIKit

class ContactCell: UITableViewCell {
  @IBOutlet weak var fullNameLabel: UILabel!
  @IBOutlet weak var adddressLabel: UILabel!
}
