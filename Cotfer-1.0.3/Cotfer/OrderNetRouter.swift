//
//  OrderNetRouter.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire

enum OrderNetRouter: URLRequestConvertible {
  case addOrder(parameters: Parameters)
  case getOrder(parameters: Parameters)
  case updateOrder(parameters: Parameters)
  case deleteOrder(parameters: Parameters)
  case checkoutOrder(parameters: Parameters)
  case getCartExist(parameters: Parameters)
  
  static let baseURLString = BASE_API_URL
  
  var method: HTTPMethod {
    switch self {
    case .addOrder, .getOrder, .updateOrder, .deleteOrder, .checkoutOrder, .getCartExist:
      return .post
    }
  }
  
  var path: String {
    switch self {
    case .addOrder:
      return ADD_ORDER_URL
    case .getOrder:
      return GET_ORDER_URL
    case .updateOrder:
      return UPDATE_ORDER_URL
    case .deleteOrder:
      return DELETE_ORDER_URL
    case .checkoutOrder:
      return CHECKOUT_ORDER_URL
    case .getCartExist:
      return GET_CART_EXIST_URL
    }
  }
  
  // MARK: URLRequestConvertible
  
  func asURLRequest() throws -> URLRequest {
    let url = try OrderNetRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .addOrder(let parameters), .getOrder(let parameters), .updateOrder(let parameters), .deleteOrder(let parameters), .checkoutOrder(let parameters), .getCartExist(let parameters):
      do {
        urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
      } catch {
      }
      urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
      urlRequest.setValue("crm", forHTTPHeaderField: "x-provider")
    }
    return urlRequest
  }
}

