//
//  HomeRouter.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import SideMenu

@objc protocol HomeRoutingLogic
{
  func routeToshowCatalogueVC(segue: UIStoryboardSegue?)
  func routeToshowCustomersVC(segue: UIStoryboardSegue?)
  func routeToshowEnterCustomerVC(segue: UIStoryboardSegue?)
  func routeToshowSideMenuVC(segue: UIStoryboardSegue?)
}

protocol HomeDataPassing
{
  var dataStore: HomeDataStore? { get }
}

class HomeRouter: NSObject, HomeRoutingLogic, HomeDataPassing
{
  weak var viewController: HomeViewController?
  var dataStore: HomeDataStore?
  
  // MARK: Routing
  
  func routeToshowCatalogueVC(segue: UIStoryboardSegue?)
  {
    if let segue = segue {
      let catalogueVC = segue.destination as! CatalogueViewController
      catalogueVC.CrmInfo = viewController?.CrmInfo
      catalogueVC.UserInfo = viewController?.UserInfo
    }
  }
  
  func routeToshowCustomersVC(segue: UIStoryboardSegue?){
    if let segue = segue {
      let customersVC = segue.destination as! CustomersViewController
      customersVC.UserInfo = viewController?.UserInfo
      customersVC.CrmInfo = viewController?.CrmInfo
      customersVC.isSideMenuClicked = false
    }
  }

  func routeToshowEnterCustomerVC(segue: UIStoryboardSegue?) {
    if let segue = segue {
      let enterCustomerVC = segue.destination as! EnterCustomerViewController
      enterCustomerVC.CrmInfo = viewController?.CrmInfo
      enterCustomerVC.UserInfo = viewController?.UserInfo
    }
  }
  
  func routeToshowSideMenuVC(segue: UIStoryboardSegue?) {
      if let segue = segue {
        let sideMenuVC = (segue.destination as! UISideMenuNavigationController).topViewController as! SideMenuViewController
        sideMenuVC.CrmInfo = viewController?.CrmInfo
        sideMenuVC.UserInfo = viewController?.UserInfo
      }
  }
}
