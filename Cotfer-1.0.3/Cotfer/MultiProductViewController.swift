//
//  MultiProductViewController.swift
//  Cotfer
//
//  Created by goran on 11/8/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import SwiftSpinner

protocol MultiProductDisplayLogic: class
{
    func display(viewModel: MultiProduct.ViewModel)
    func displayOrder(viewModel: MultiProduct.ViewModel)
}

class MultiProductViewController: UIViewController, MultiProductDisplayLogic, BaseProtocol
{
    var interactor: MultiProductBusinessLogic?
    var router: (NSObjectProtocol & MultiProductRoutingLogic & MultiProductDataPassing)?
    var request = MultiProduct.Request()
    var quantityTextStr: String = "0.00"
    var uniteCodeText: String = ""
    var clientPriceText: String = ""
    var productDescText: String = ""
    var isUpdateCart: Bool!
    var product: ResponseProductItem!
    var managedProduct: ManagedProduct!
    var imageInfo: ResponseImagesInfo!
    var UserInfo: ResponseUserInfo!
    var CrmInfo: CrmInfo!
    var Employee: Employee!
    var delegateUser: RefreshUserInfo!
    var quantity: Double!
    var contact: ResponseContact!
    var customer: ResponseClient!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var addToOrderButton: UIButton!
    @IBOutlet weak var productStockLabel: UILabel!
    @IBOutlet weak var totalPriceValueLabel: UILabel!
    @IBOutlet weak var currencyLabel: UILabel!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productDescriptionLabel: UILabel!
    @IBOutlet weak var productDescTextField: UITextField!
    @IBOutlet weak var clientPriceTextField: UITextField!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var isEditedDesc = false
    var isEditedPrice = false
    var isEditedQty = false
    let userDefaults = UserDefaults.standard
    var products: [ResponseProductItem] = []
    var productsToPutInCart  : [ResponseProductItem] = []
    var productsEdited = 0
    var index = 0
    var activeTextField : UITextField!
    var viewStatus: Int?
    
    var isSideMenuClicked  = false
    // MARK: Object lifecycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
    {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        setup()
    }
    
    // MARK: Setup
    
    private func setup()
    {
        let viewController = self
        let interactor = MultiProductInteractor()
        let presenter = MultiProductPresenter()
        let router = MultiProductRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }
    
    // MARK: Routing
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if let scene = segue.identifier {
            let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
            if let router = router, router.responds(to: selector) {
                router.perform(selector, with: segue)
            }
        }
    }
    
    // MARK: View lifecycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        if let employeeData = userDefaults.object(forKey: "employeeModel") {
            Employee = NSKeyedUnarchiver.unarchiveObject(with: employeeData as! Data) as! Employee
        }
        
        viewStatus = 0
        changeWindowBackgroundColor()
        
        if isInternetAvailable() {
            request.UserInfo = UserInfo
            request.CrmInfo = CrmInfo
            request.DeliveryClientId = customer.ClientId
            request.ContactId = contact.ContactId
            request.employeeId = Employee.EmployeeId
            request.isEditedDesc = isEditedDesc
            request.isEditedPrice = isEditedPrice
            request.isEditedQty = isEditedQty
            request.ProductDescription = product.Description
            request.Skip = 0
            var urlString = imageInfo.ImgPathNormal! + product.ImageName!
            if  urlString.contains(" ") {
                urlString = urlString.replacingOccurrences(of: " ", with: "%20")
            }
            let url = URL(string: urlString)
            productImageView.kf.setImage(with: url, placeholder: UIImage.init(named: "shop_default"), options: nil, progressBlock: nil, completionHandler: nil)
            productNameLabel.text = product.ShortName
            productDescTextField.text = product.Description
            productStockLabel.text = product.ProductCode
            descLabel.text = NSLocalizedString("description", comment: "")
            totalPriceValueLabel.text = "\(product.QtyStockAvailableAtTerm!)"
            currencyLabel.text = "\(product.QtyStockAvailableImmediately!)"
        }else{
            productNameLabel.text = managedProduct.productTitle
            productDescriptionLabel.text = managedProduct.productDescription
            productStockLabel.text = managedProduct.productId
        }
        request.Filter = "FilterActive/1/FilterManageStock/1/FilterCatalog/1/ShortName/" + product.ShortName!
        SwiftSpinner.show(NSLocalizedString("Loading", comment: ""))
        setupUI()
        addObserversToKeyboardState()
        
        sortProducts()
        tableView.reloadData()
    }
    
    func addObserversToKeyboardState() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidHide(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidShow(notification:)),
                                               name: NSNotification.Name.UIKeyboardDidShow, object: nil)
    }
    
    func setupUI() {
        addToOrderButton.setTitle(NSLocalizedString("addorder", comment: ""), for: .normal)
    }
    
    func sortProducts() {
        products = products.sorted {
            ($0.CatSort9! as NSString).integerValue < ($1.CatSort9! as NSString).integerValue
        }
        print("Tuka")
    }
    
    // MARK: Do something
    func getProducts() {
        interactor?.getProducts(request: request)
    }
    
    func updateOrder(product: ResponseProductItem){
        if index == 0 {
            SwiftSpinner.show(NSLocalizedString("Loading", comment: ""))
        }
       
        if quantityTextStr.isEmpty && quantityTextStr.characters.count == 0 && quantityTextStr.characters.count < 4 {
            showAlert(message: NSLocalizedString("You don't set any number, please enter some value", comment: ""))
        } else {
            request.Quantity = Double(product.UnitQty!)
            request.UnitPrice = String(product.Price!)
            request.isEditedPrice = product.isEditedPrice
            request.ProductRef = product.ProductCode
            if productDescTextField.text != self.product.Description {
                request.isEditedDesc = true
                if let productDesc = productDescTextField.text {
                    request.ProductDescription = productDesc
                    productDescText = productDesc
                }
            } else {
                request.isEditedDesc = false
            }
            
            print("Product"  + product.ProductCode!)
            interactor?.addOrder(request: request)
        }
    }
    
    func createOrder(quantity: Double, productRef: String) {
        request.ProductRef = productRef
        request.ProductDescription = managedProduct.productDescription
        request.ProductName = managedProduct.productTitle
        request.Quantity = quantity
        request.PriceCurrency = managedProduct.priceCurrency
        request.lineId = Int(arc4random_uniform(10000))
        request.TotalPrice = Double(managedProduct.productPrice) *  quantity
        interactor?.createOrder(request: request)
    }
    
    func display(viewModel: MultiProduct.ViewModel)
    {
        SwiftSpinner.hide()
        if viewModel.viewModel != nil {
            products = viewModel.viewModel.ProductsList!
        }
        tableView.reloadData()
    }
    
    func displayOrder(viewModel: MultiProduct.ViewModel) {
        if viewModel.viewModelOrder != nil {
            UserInfo = viewModel.viewModelOrder.ResponseUserInfo!
            let userInfoModel = ResponseUserInfo.init(userInfo: viewModel.viewModelOrder.ResponseUserInfo!)
            let encodedDataUser = NSKeyedArchiver.archivedData(withRootObject: userInfoModel)
            userDefaults.set(encodedDataUser, forKey: "userInfoModel")
            if index < productsToPutInCart.count - 1 {
                index += 1
                updateOrder(product: productsToPutInCart[index])
                if index == productsToPutInCart.count - 1 {
                    request.ProductDescription = productDescTextField.text
                }
            } else {
                SwiftSpinner.hide()
                let count = self.navigationController?.viewControllers.count
                let selectCatalogueVC = self.navigationController?.viewControllers[count! - 2] as! SelectCatalogueViewController
                selectCatalogueVC.addedOrder = true
                navigationController?.popViewController(animated: false)
            }
        }
    }
    
    @IBAction func addOrderClicked(_ sender: Any) {
        if isInternetAvailable() {
            var i = 0
            var productsEdit = false
            for product in products {
                if product.isEdited {
                    i += 1
                    productsEdit = true
                    productsToPutInCart.append(product)
                }
            }
            
            if !productsEdit {
                showAlert(message: NSLocalizedString("You don't set any number, please enter some value", comment: ""))
            } else {
                self.updateOrder(product: productsToPutInCart[index])
            }
        }else{
            //createOrder(quantity: Double(quantityTextField.text!)!, productRef: managedProduct.productId!)
        }
    }
    
    //MARK : Keyboard functions
    func keyboardDidHide(notification: NSNotification) {
        if viewStatus == 1 {
            animateViewMoving(false, moveValue: 350)
            activeTextField.resignFirstResponder()
        }
        viewStatus = 0
    }
    
    func keyboardDidShow(notification: NSNotification) {
        let info = notification.userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue.size
        if viewStatus == 0 {
            animateViewMoving(true, moveValue: 350)
            viewStatus = 1
        } else {
            if keyboardSize?.height == 471 {
                animateViewMoving(false, moveValue: 350)
                activeTextField.resignFirstResponder()
                viewStatus = 0
            } else {
                viewStatus = 1
            }
        }
    }
    
    func changeWindowBackgroundColor() {
        let win:UIWindow = UIApplication.shared.delegate!.window!!
        win.backgroundColor = UIColor.white
    }
}

extension MultiProductViewController: UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count > 0 ? products.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MultiProductCell", for: indexPath) as! MultiProductCell
        var product = products[indexPath.row]
        
        print("Size : \(product.Cat9!) SortNumber : \(product.CatSort9!)")
        cell.sizeLabel.text = NSLocalizedString("size", comment: "")
       // let tempArray = product.Description!.components(separatedBy: CharacterSet.newlines)
        cell.sizeValueLabel.text = product.Cat9
        cell.qtyLabel.text = NSLocalizedString("qty", comment: "")
        let modified = product.UnitCode?.replace(target: "/", withString:"")
        cell.qtyTextField.text = product.isEdited ? "\(product.UnitQty!)" : "0"
        cell.stepper.value = product.isEdited ? Double(product.UnitQty!) : 0.0
        cell.qtyTextField.tag = 10000 + indexPath.row
        cell.packageLabel.text = NSLocalizedString("packaging", comment: "")
        cell.packageValueLabel.text = product.UnitCode!
        cell.lblSTD.text = "STD: \(String(describing: product.QtyStockAvailableImmediately!))"
        cell.lblStockAtTerm.text = "SAT: \(String(describing: product.QtyStockAvailableAtTerm!))"
        cell.priceLabel.text = NSLocalizedString("price", comment: "")
        cell.priceTextField.text = String(format: "%.2f", product.Price!)
        cell.priceTextField.tag = 20000 + indexPath.row
        cell.stepper.stepValue = Double((modified?.isEmpty)! ? "1.0": modified!)!
        cell.stepperClick = { [weak self](cell) in
            if cell.qtyTextField.tag.array.first == 1 {
                let tagCount = cell.qtyTextField.tag.array.count
                let index = cell.qtyTextField.tag.array.last! + cell.qtyTextField.tag.array[tagCount - 2] * 10
                product = (self?.products[index])!
              //  var  modified = product.UnitCode?.replace(target: "/", withString:"")
              //  modified = (modified?.isEmpty)! ? "1": modified
                cell.qtyTextField.text = "\(Int(cell.stepper.value))"
                let quantityText = cell.qtyTextField.text!
                if  cell.stepper.value != 0 {
                    product.isEdited = true
                    self?.isEditedQty = true
                    product.UnitQty = Int(quantityText)
                    self?.request.isEditedQty = self?.isEditedQty
                    self?.request.Quantity = Double(quantityText)
                } else {
                    product.isEdited = false
                    product.UnitQty = 0
                }
            }
        }
        return cell
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
       // animateViewMoving(true, moveValue: 350)
        return true
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
       // animateViewMoving(false, moveValue: 350)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.tag.array.first == 1 {
            let tagCount = textField.tag.array.count
            let index = textField.tag.array.last! + textField.tag.array[tagCount - 2] * 10
            let product = products[index]
            product.UnitQty = Int(textField.text!)
            product.isEdited = true
            isEditedDesc = true
            request.isEditedDesc = isEditedDesc
        }else if textField.tag.array.first == 2 {
            let tagCount = textField.tag.array.count
            let index = textField.tag.array.last! + textField.tag.array[tagCount - 2] * 10
            let product = products[index]
            product.Price = Double(textField.text!)
            product.isEdited = true
            product.isEditedPrice = true
            isEditedPrice = true
            request.isEditedDesc = isEditedDesc
        }else {
            product.isEdited = true
            isEditedDesc = true
            request.isEditedDesc = isEditedDesc
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.tag.array.first == 1 {
            let tagCount = textField.tag.array.count
            let index = textField.tag.array.last! + textField.tag.array[tagCount - 2] * 10
            let product = products[index]
            if textField.text! != "0" {
                product.UnitQty = Int(textField.text!)
                product.isEdited = true
            }
        }else if textField.tag.array.first == 2 {
            let tagCount = textField.tag.array.count
            let index = textField.tag.array.last! + textField.tag.array[tagCount - 2] * 10
            let product = products[index]
            product.Price = Double(textField.text!)
            product.isEdited = true
            product.isEditedPrice = true
            request.isEditedDesc = isEditedDesc
        }else {
            product.isEdited = true
            isEditedDesc = true
            request.isEditedDesc = isEditedDesc
        }
    }
}
