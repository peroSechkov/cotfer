//
//  ResponseCartExist.swift
//  Cotfer
//
//  Created by goran on 10/30/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseCartExist: Mappable {
  var ExistingCartId: Int?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ExistingCartId <- map["ResponseData.ExistingCartId"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
