//
//  ResponseCustomer.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseClient: NSObject, Mappable, NSCoding {
  
  var ClientId: Int?
  var InvoiceClientId: Int?
  var GroupClientId: Int?
  var ClientGroupFlag: Int?
  var ClientInvoiceFlag: Int?
  var ClientDeliveryFlag: Int?
  var Civility: String?
  var CompanyName: String?
  var CompanyName2: String?
  var Address: String?
  var Address2: String?
  var Zip: String?
  var City: String?
  var Country: String?
  var Region: String?
  var PhoneNumber: String?
  var PhoneNumber2: String?
  var FaxNumber: String?
  var MobileNumber: String?
  var MobileNumber2: String?
  var ConsultantId: String?
  var VendorId: String?
  var EmployeeId: String?
  var ClientInfo: String?
  var ActionInfo: String?
  var WebSite: String?
  var Email: String?
  var StatusId: Int?
  var StatusDate: String?
  var StatusTime: String?
  var CreditAuthorization: Int?
  var CreditLimit: Int?
  var ClientCat1: String?
  var ClientCat2: String?
  var ClientCat3: String?
  var ClientCat4: String?
  var ClientCat5: String?
  var CreationDate: String?
  var ModifyDate: String?
  var DeactivationDate: String?
  var DeliveryInfo: String?
  var AttentionInfo: String?
  var PaymentTermId: Int?
  var TransporterId: Int?
  var DiscountPro: Int?
  var PriceCategory: Int?
  var ClientTaxId: Int?
  var ClientTaxInc: Int?
  var ClientCurrency: String?
  var WithShippingCosts: Int?
  var ClassClientId: Int?
  var ClassClientDescr: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  
  init(clientId:Int, companyName: String, email: String, address: String, country: String, phone: String, role: String){
    self.ClientId = clientId
    self.CompanyName = companyName
    self.Email = email
    self.Address = address
    self.Country = country
    self.PhoneNumber = phone
    self.EmployeeId = role
  }
  
  required init?(map: Map) {}
  
  required init(coder decoder: NSCoder) {
    self.ClientId = decoder.decodeObject(forKey: "ClientId") as? Int ?? 0
    self.CompanyName = decoder.decodeObject(forKey: "CompanyName") as? String ?? ""
    self.Email = decoder.decodeObject(forKey: "Email") as? String ?? ""
    self.Address = decoder.decodeObject(forKey: "Address") as? String ?? ""
    self.Country = decoder.decodeObject(forKey: "Country") as? String ?? ""
    self.PhoneNumber = decoder.decodeObject(forKey: "PhoneNumber") as? String ?? ""
    self.EmployeeId = decoder.decodeObject(forKey: "EmployeeId") as? String ?? ""
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(ClientId, forKey: "ClientId")
    aCoder.encode(CompanyName, forKey: "CompanyName")
    aCoder.encode(Email, forKey: "Email")
    aCoder.encode(Address, forKey: "Address")
    aCoder.encode(Country, forKey: "Country")
    aCoder.encode(PhoneNumber, forKey: "PhoneNumber")
    aCoder.encode(EmployeeId, forKey: "EmployeeId")
  }
  
  func mapping(map: Map) {
    ClientId <- map["ClientId"]
    InvoiceClientId <- map["InvoiceClientId"]
    GroupClientId <- map["GroupClientId"]
    ClientGroupFlag <- map["ClientGroupFlag"]
    ClientInvoiceFlag <- map["ClientInvoiceFlag"]
    ClientDeliveryFlag <- map["ClientDeliveryFlag"]
    Civility <- map["Civility"]
    CompanyName <- map["CompanyName"]
    CompanyName2 <- map["CompanyName2"]
    Address <- map["Address"]
    Address2 <- map["Address2"]
    Zip <- map["Zip"]
    City <- map["City"]
    Country <- map["Country"]
    Region <- map["Region"]
    PhoneNumber <- map["PhoneNumber"]
    PhoneNumber2 <- map["PhoneNumber2"]
    FaxNumber <- map["FaxNumber"]
    MobileNumber <- map["MobileNumber"]
    MobileNumber2 <- map["MobileNumber2"]
    ConsultantId <- map["ConsultantId"]
    VendorId <- map["VendorId"]
    EmployeeId <- map["EmployeeId"]
    ClientInfo <- map["ClientInfo"]
    ActionInfo <- map["ActionInfo"]
    WebSite <- map["WebSite"]
    Email <- map["Email"]
    StatusId <- map["StatusId"]
    StatusDate <- map["StatusDate"]
    StatusTime <- map["StatusTime"]
    CreditAuthorization <- map["CreditAuthorization"]
    CreditLimit <- map["CreditLimit"]
    ClientCat1 <- map["ClientCat1"]
    ClientCat2 <- map["ClientCat2"]
    ClientCat3 <- map["ClientCat3"]
    ClientCat4 <- map["ClientCat4"]
    ClientCat5 <- map["ClientCat5"]
    CreationDate <- map["CreationDate"]
    ModifyDate <- map["ModifyDate"]
    DeactivationDate <- map["DeactivationDate"]
    DeliveryInfo <- map["DeliveryInfo"]
    AttentionInfo <- map["AttentionInfo"]
    PaymentTermId <- map["PaymentTermId"]
    TransporterId <- map["TransporterId"]
    DiscountPro <- map["DiscountPro"]
    PriceCategory <- map["PriceCategory"]
    ClientTaxId <- map["ClientTaxId"]
    ClientTaxInc <- map["ClientTaxInc"]
    ClientCurrency <- map["ClientCurrency"]
    WithShippingCosts <- map["WithShippingCosts"]
    ClassClientId <- map["ClassClientId"]
    ClassClientDescr <- map["ClassClientDescr"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}

extension ResponseClient {
  convenience init(managedObject: ManagedCustomer) {
    self.init(clientId: Int(managedObject.clientId), companyName: managedObject.companyName!, email: managedObject.email! ,address: managedObject.address!, country: managedObject.country!, phone: managedObject.phone!, role: managedObject.role!)
  }
}

