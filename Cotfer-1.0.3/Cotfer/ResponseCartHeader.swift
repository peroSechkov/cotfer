//
//  ResponseCartHeader.swift
//  RVGPeem
//
//  Created by goran on 6/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseCartHeader: Mappable {
  
  var CartId: Int?
  var InvoiceClient: Int?
  var DeliveryClient: Int?
  var PaymentTermId: Int?
  var TransporterId: Int?
  var InvoiceTo: String?
  var DeliveryTo: String?
  var DeliveryDate: String?
  var DeliveryInfo: String?
  var OurReference: String?
  var Comment: String?
  var TotalQty: Double?
  var TaxInc: Double?
  var TotalProducts: Int?
  var TotalShipping: Double?
  var TotalDiscountCode: Double?
  var TotalWeight: Double?
  var TotalElement: Int?
  var TotalCart: Double?
  var TotalCartTax: Double?
  var Currency: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CartId <- map["CartId"]
    InvoiceClient <- map["InvoiceClient"]
    DeliveryClient <- map["DeliveryClient"]
    DeliveryTo <- map["DeliveryTo"]
    InvoiceTo <- map["InvoiceTo"]
    PaymentTermId <- map["PaymentTermId"]
    TransporterId <- map["TransporterId"]
    DeliveryDate <- map["DeliveryDate"]
    DeliveryInfo <- map["DeliveryInfo"]
    Comment <- map["Comment"]
    OurReference <- map["OurReference"]
    TotalQty <- map["TotalQty"]
    TaxInc <- map["TaxInc"]
    TotalProducts <- map["TotalProducts"]
    TotalShipping <- map["TotalShipping"]
    TotalDiscountCode <- map["TotalDiscountCode"]
    TotalWeight <- map["TotalWeight"]
    TotalElement <- map["TotalElement"]
    TotalCart <- map["TotalCart"]
    TotalCartTax <- map["TotalCartTax"]
    Currency <- map["Currency"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
