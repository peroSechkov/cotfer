//
//  ResponseLogin.swift
//  Cotfer
//
//  Created by goran on 9/18/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseLogin: Mappable {
  
  var CrmInfo: CrmInfo?
  var ResponseUserInfo: ResponseUserInfo?
  var ResponseInfo: ResponseInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    CrmInfo <- map["ResponseData.CrmInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
    ResponseInfo <- map["ResponseInfo"]
  }
}
