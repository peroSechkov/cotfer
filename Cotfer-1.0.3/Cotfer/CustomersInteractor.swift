
//
//  CustomersInteractor.swift
//  Cotfer
//
//  Created by goran on 9/1/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import RxSwift
import Alamofire
import CoreStore

protocol CustomersBusinessLogic
{
  func getCustomers(request: Customers.Request)
  func getContacts(request: Customers.Request)
  func getProducts(request: Customers.Request)
  func createProduct(request: Customers.Request)
  func createContact(request: Customers.Request)
  func createCustomer(customerData : ResponseClient)
  func createImageInfo(request: Customers.Request)
  func getDeliveryList(request: Customers.Request)
  func createDeliveryInfo(request: Customers.Request)
    
  func getCustomersFromCoreData()
}

protocol CustomersDataStore {

}

class CustomersInteractor: CustomersBusinessLogic, CustomersDataStore
{
  
  var presenter: CustomersPresentationLogic?
  var worker: CustomerWorker = CustomerWorker(customerStore: CustomerService())
  let disposeBag: DisposeBag = DisposeBag()
  var response = Customers.Response()

  let guid = UIDevice.current.identifierForVendor?.uuidString
  // MARK: Do something
  
    
  func getCustomersFromCoreData() {
     weak var weakSelf = self
    worker.fetchStoredCustomers({ (managedCustomers,error) in
        if error == nil {
            weakSelf?.presenter?.presentCustomersFromCoreData(customers: managedCustomers)
        }
    })
  }
    
  func createImageInfo(request: Customers.Request) {
    let image = ResponseImagesInfo(productId: request.productId!, image: request.picture)
    weak var weakSelf = self
    worker.storeImage(image, completionHandler: { (error) -> Void in
      weakSelf?.response.responseImage = image
      weakSelf?.presenter?.presentImage(response: (weakSelf?.response)!)
    })
  }
  
  func createCustomer(customerData : ResponseClient) {
    worker.storeCustomer(customerData, completionHandler: {_ in
        print("ItemStored")
    })
  }
    
  func createProduct(request: Customers.Request)
  {
    let product = ResponseProductItem(productId: request.productId, customerPrice: request.customerPrice, productTitle: request.productTitle,  productDescription: request.productDescription, productPrice: request.productPrice,productPrice2: request.productPrice2,productPrice3: request.productPrice3,productPrice4: request.productPrice4,productPrice5: request.productPrice5,productPrice6: request.productPrice6,productPrice7: request.productPrice7,productPrice8: request.productPrice8,productPrice9: request.productPrice9,productPrice10: request.productPrice10,productQty1: request.productQty1,productQty2: request.productQty2,productQty3: request.productQty3,productQty4: request.productQty4,productQty5: request.productQty5,productQty6: request.productQty6,productQty7: request.productQty7,productQty8: request.productQty8,productQty9: request.productQty9,productQty10: request.productQty10,barcode: request.barcode, unitCode: request.unitCode, capacity: request.capacity, priceCurrency: request.priceCurrency,QtyStockAvailableImmediately : request.QtyStockAvailableImmediately!,QtyStockAvailableAtTerm : request.QtyStockAvailableAtTerm!,cat1 : request.category1,cat2 : request.category2)
    weak var weakSelf = self
    worker.storeProduct(product) { (error) -> Void in
      weakSelf?.response.responseProduct = product
      weakSelf?.presenter?.presentProduct(response: (weakSelf?.response)!)
    }
  }
  
  func createContact(request: Customers.Request) {
    let contact = ResponseContact(contactId: request.contactId, firstName: request.firstName, lastName: request.lastName, email: request.email, phone: request.phone, role: request.role, address: request.address)
    weak var weakSelf = self
    worker.storeContact(contact) { (error) -> Void in
      weakSelf?.response.responseContact = contact
      weakSelf?.presenter?.presentContact(response: (weakSelf?.response)!)
    }
  }
  
  func createDeliveryInfo(request: Customers.Request)
  {
    let delivery = DeliveryClient(clientId: request.clientId, address: request.address, country: request.country, phone: request.phone, zip: request.zip, city: request.city,companyName : request.companyName)
    weak var weakSelf = self
    worker.storeDelivery(delivery) { (error) -> Void in
      weakSelf?.response.responseDelivery = delivery
      weakSelf?.presenter?.presentDelivery(response: (weakSelf?.response)!)
    }
  }
  
  func getDeliveryList(request: Customers.Request)
  {
    weak var weakSelf = self
    let ShopInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Server": request.CrmInfo.Server!,
        "Db": request.CrmInfo.SBddName!,
        "DbUser": request.CrmInfo.DbUser!,
        "DbPass": request.CrmInfo.DbPass!
        ] as [String : Any]
    let UserInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "ClientNo": request.clientId!,
        "LoginNo": request.UserInfo.LoginNo!,
        "EmployeeId": request.UserInfo.EmployeeId!,
        "Language": Language,
        "CartNo":  request.UserInfo.CartNo!
        ] as [String : Any]
    let RequestInfo =
      [
        "ClientId": request.clientId!
      ] as [String : Any]
    let parameters: Parameters = ["RequestInfo": RequestInfo, "ShopInfo": ShopInfo, "UserInfo": UserInfo]
    worker.getDeliveryInfo(parameters: parameters).asObservable().subscribe(
      onNext: { data in
        do {
          weakSelf?.response.responseAddress = data
          weakSelf?.presenter?.presentAddress(response: (weakSelf?.response)!)
        }
    },
      onError: { error in
        weakSelf?.presenter?.presentAddress(response: (weakSelf?.response)!)
    },
      onCompleted: {
        print("Completed")
    },
      onDisposed: {
        print("Disposed")
    })
      .addDisposableTo(disposeBag)
  }
  
  func getContacts(request: Customers.Request)
  {
    weak var weakSelf = self
    let ShopInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Server": request.CrmInfo.Server!,
        "Db": request.CrmInfo.SBddName!,
        "DbUser": request.CrmInfo.DbUser!,
        "DbPass": request.CrmInfo.DbPass!
        ] as [String : Any]
    let UserInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Language": Language
        ] as [String : Any]
    let RequestInfo =
      [
        "ClientId": request.clientId,
        "OrderBy": OrderBy,
        "Skip": request.skip,
        "Take": Take
        ] as [String : Any]
    let parameters: Parameters = ["ShopInfo": ShopInfo, "UserInfo": UserInfo, "RequestInfo": RequestInfo]
    
    worker.getContacts(parameters: parameters).asObservable().subscribe(
      onNext: { data in
        do {
          weakSelf?.response.responseContacts = data
          weakSelf?.presenter?.presentContacts(response: (weakSelf?.response)!)
        }
    },
      onError: { error in
        weakSelf?.presenter?.present(response: (weakSelf?.response)!)
    },
      onCompleted: {
        print("Completed")
    },
      onDisposed: {
        print("Disposed")
    })
      .addDisposableTo(disposeBag)
  }
  
  func getProducts(request: Customers.Request)
  {
    weak var weakSelf = self
    let ShopInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Server": request.CrmInfo.Server!,
        "Db": request.CrmInfo.SBddName!,
        "DbUser": request.CrmInfo.DbUser!,
        "DbPass": request.CrmInfo.DbPass!,
        "CrmName": Code,
        "CrmGuid": guid!
        ] as [String : Any]
    let UserInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "ClientNo": request.UserInfo.ClientNo!,
        "LoginNo": request.UserInfo.LoginNo!,
        "EmployeeId": request.UserInfo.EmployeeId!,
        "Language": Language,
        "CartNo":  request.UserInfo.CartNo!
        ] as [String : Any]
    let RequestInfo =
      [
        "ClientId": request.clientId!,
        "OrderBy": OrderBy,
        "Skip": Skip,
        "Take": 0,
        "Filter": request.filter
        ] as [String : Any]
    let parameters: Parameters = ["ShopInfo": ShopInfo, "UserInfo": UserInfo, "RequestInfo": RequestInfo]
    worker.getAllProducts(parameters: parameters).asObservable().subscribe(
      onNext: { data in
        do {
          weakSelf?.response.responseProducts = data
          weakSelf?.presenter?.presentProducts(response: (weakSelf?.response)!)
        }
    },
      onError: { error in
        weakSelf?.presenter?.present(response: (weakSelf?.response)!)
    },
      onCompleted: {
        print("Completed")
    },
      onDisposed: {
        print("Disposed")
    })
      .addDisposableTo(disposeBag)
  }
  
  func getCustomers(request: Customers.Request)
  {
    // NOTE: Create some Worker to do the work
    weak var weakSelf = self
    var searchStr = ""
    if let searchString = request.searchString {
      searchStr = searchString
    }
    let RequestInfo =
      [
       "SearchString": searchStr,
       "OrderBy": OrderBy ,
       "Skip": request.skip,
       "Take": 0
      ] as [String : Any]
    let ShopInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Server": request.CrmInfo.Server!,
        "Db": request.CrmInfo.SBddName!,
        "DbUser": request.CrmInfo.DbUser!,
        "DbPass": request.CrmInfo.DbPass!
      ] as [String : Any]
    let UserInfo =
      [
        "ShopNumber": request.CrmInfo.ShopNumber!,
        "Language": Language
      ] as [String : Any]
    
    let parameters: Parameters = ["RequestInfo": RequestInfo, "ShopInfo": ShopInfo, "UserInfo": UserInfo]
    print(parameters)
    worker.getClients(parameters: parameters).asObservable().subscribe(
      onNext: { data in
        do {
          weakSelf?.response.response = data
          weakSelf?.presenter?.present(response: (weakSelf?.response)!)
        }
    },
      onError: { error in
        weakSelf?.presenter?.present(response: (weakSelf?.response)!)
    },
      onCompleted: {
        print("Completed")
    },
      onDisposed: {
        print("Disposed")
    })
      .addDisposableTo(disposeBag)
  }
}
