//
//  Extensions.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import SwiftKeychainWrapper
import SystemConfiguration

func setManager() -> SessionManager {
  
  var Token: String = ""
  if let accessToken = KeychainWrapper.standard.string(forKey: "Token") {
    Token = accessToken
  }
  let sessionManager = SessionManager.default
  let adapter = AccessTokenAdapter(accessToken: Token)
  sessionManager.adapter = adapter
  
  return sessionManager
}

func isUserLoggedIn() -> Bool{
  let userDefaults = UserDefaults.standard
  if userDefaults.object(forKey: "isLogged") != nil {
    return true
  }
  return false
}

protocol RefreshUserInfo {
  func refreshUserInfo(userInfo: ResponseUserInfo)
}

extension String {
  func isValidEmail(emailStr:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    return emailTest.evaluate(with: emailStr)
  }
  
  func replace(target: String, withString: String) -> String {
    return self.replacingOccurrences(of: target, with: withString, options: NSString.CompareOptions.literal, range: nil)
  }
}

extension Int {
  var array: [Int] {
    return String(self).characters.flatMap{ Int(String($0)) }
  }
}

protocol BaseProtocol {}

extension BaseProtocol where Self: UIViewController {
  
  func showAlert(title: String = NSLocalizedString("Error", comment: ""), message: String) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alertController.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: nil))
    present(alertController, animated: true, completion: nil)
  }
  
  func showLogoutAlert(title: String, message: String,  completion: @escaping (_ result: Bool)->()) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: {
      alert -> Void in
      weak var weakSelf = self
      completion(true)
      weakSelf?.dismiss(animated: true, completion: nil)
    })
    let cancelAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: .default, handler: {
      (action : UIAlertAction!) -> Void in
      weak var weakSelf = self
      weakSelf?.dismiss(animated: true, completion: nil)
    })
    alertController.addAction(okAction)
    alertController.addAction(cancelAction)
    present(alertController, animated: true, completion: nil)
  }
  
  func animateViewMoving (_ up:Bool, moveValue :CGFloat){
    let movementDuration:TimeInterval = 0.3
    let movement:CGFloat = ( up ? -moveValue : moveValue)
    UIView.beginAnimations( "animateView", context: nil)
    UIView.setAnimationBeginsFromCurrentState(true)
    UIView.setAnimationDuration(movementDuration )
    self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement)
    UIView.commitAnimations()
  }
  
  func hideNavigationButton(){
    let backButton = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: navigationController, action: nil)
    navigationItem.leftBarButtonItem = backButton
  }
  
  func isInternetAvailable() -> Bool
  {
    var zeroAddress = sockaddr_in()
    zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
    zeroAddress.sin_family = sa_family_t(AF_INET)
    
    let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
      $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
        SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
      }
    }
    
    var flags = SCNetworkReachabilityFlags()
    if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
      return false
    }
    
    let isReachable = flags.contains(.reachable)
    let needsConnection = flags.contains(.connectionRequired)
    return (isReachable && !needsConnection)
  }
}


