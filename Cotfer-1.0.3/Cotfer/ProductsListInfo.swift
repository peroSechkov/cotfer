//
//  ProductsListInfo.swift
//  RVGPeem
//
//  Created by goran on 7/3/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct  ProductsListInfo: Mappable {

  var ClientId: Int?
  var OrderBy: Int?
  var Skip: Int?
  var Take: Int?
  var Filter: String?
  var Total: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ClientId <- map["ClientId"]
    OrderBy <- map["OrderBy"]
    Skip <- map["Skip"]
    Take <- map["Take"]
    Filter <- map["Filter"]
    Total <- map["Total"]
  }
}
