//
//  CustomerWorker.swift
//  Cotfer
//
//  Created by goran on 7/26/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import RxSwift
import CoreStore

class CustomerWorker
{
  var customerStore: CustomerStoreProtocol
  
  init(customerStore: CustomerStoreProtocol) {
    self.customerStore = customerStore
  }
  
  func getClients(parameters: Parameters) -> Observable<ResponseClientList> {
    return customerStore.getClients(parameters: parameters)
  }
  
  func getContacts(parameters: Parameters) -> Observable<ResponseContactList> {
    return customerStore.getContacts(parameters: parameters)
  }
  
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts> {
    return customerStore.getAllProducts(parameters: parameters)
  }
  
  func getDeliveryInfo(parameters: Parameters) -> Observable<ResponseDeliveryList>{
    return customerStore.getDeliveryInfo(parameters: parameters)
  }
    
  func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList> {
    return customerStore.addOrder(parameters: parameters)
  }
  
  func customerRoles(parameters: Parameters) -> Observable<ResponseClient> {
    return customerStore.customerRoles(parameters: parameters)
  }
  
  func fetchStoredCustomers(_ completionHandler: @escaping (_ customers: [ManagedCustomer], _ error: CustomersOperationError?) -> Void){
    customerStore.fetchStoredCustomers { (customers: [ManagedCustomer], error) -> Void in
      completionHandler(customers, error)
    }
  }
  
  func fetchStoredDeliveries(_ completionHandler: @escaping (_ deliveries: [ManagedDelivery], _ error: CustomersOperationError?) -> Void){
    customerStore.fetchStoredDeliveries { (deliveries: [ManagedDelivery], error) -> Void in
      completionHandler(deliveries, error)
    }
  }

  func fetchCustomer(_ id: String, completionHandler: @escaping (_ customer: ResponseClient?, _ error: CustomersOperationError?) -> Void) {
    customerStore.fetchCustomer(id) {(customer, error) -> Void in
      completionHandler(customer, error)
    }
  }
  
  func fetchDelivery(_ id: String, completionHandler: @escaping (_ customer: DeliveryClient?, _ error: CustomersOperationError?) -> Void) {
    customerStore.fetchDelivery(id) {(delivery, error) -> Void in
      completionHandler(delivery, error)
    }
  }
  
  func storeCustomer(_ customerToStore: ResponseClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.storeCustomer(customerToStore) { error in
      completionHandler(error)
    }
  }
  
  func storeDelivery(_ deliveryToStore: DeliveryClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.storeDelivery(deliveryToStore) { error in
      completionHandler(error)
    }
  }
  
  func updateCustomer(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.updateCustomer(customer: customer, completionHandler: completionHandler)
  }
  
  func updateDelivery(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.updateDelivery(delivery: delivery, completionHandler: completionHandler)
  }

  func deleteCustomers(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    
  }
  
  func deleteDeliveries(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void) {
    
  }
  
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.storeProduct(productToStore) { error in
      completionHandler(error)
    }
  }
  
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.storeContact(contactToStore) { error in
      completionHandler(error)
    }
  }
  
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) {
    customerStore.storeImage(imageToStore, completionHandler: { (error) in
      completionHandler(error)
    })
  }
}

protocol CustomerStoreProtocol {
  func getClients(parameters: Parameters) -> Observable<ResponseClientList>
  func getContacts(parameters: Parameters) -> Observable<ResponseContactList>
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts>
  func getDeliveryInfo(parameters: Parameters) -> Observable<ResponseDeliveryList>
  func addOrder(parameters: Parameters) -> Observable<ResponseCartDetailsList>
  func customerRoles(parameters: Parameters) -> Observable<ResponseClient>
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void) 
  func fetchStoredCustomers(_ completionHandler: @escaping (_ customers: [ManagedCustomer], _ error: CustomersOperationError?) -> Void)
  func fetchStoredDeliveries(_ completionHandler: @escaping (_ deliveries: [ManagedDelivery], _ error: CustomersOperationError?) -> Void)
  func fetchCustomer(_ id: String, _ completionHandler: @escaping (_ customer: ResponseClient, _ error: CustomersOperationError?) -> Void)
  func fetchDelivery(_ id: String, _ completionHandler: @escaping (_ delivery: DeliveryClient, _ error: CustomersOperationError?) -> Void)
  func storeCustomer(_ customerToStore: ResponseClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void)
  func storeDelivery(_ deliveryToStore: DeliveryClient, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void)
  func updateCustomer(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void)
  func updateDelivery(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void)
  func deleteCustomers(customer: ResponseClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void)
  func deleteDeliveries(delivery: DeliveryClient, completionHandler:  @escaping (_ error: CustomersOperationError?) -> Void)
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void)
  func storeContact(_ contactToStore: ResponseContact, completionHandler: @escaping (_ error: CustomersOperationError?) -> Void)
  }

enum CustomersStoreResult<U>
{
  case success(result: U)
  case failure(error: CustomersOperationError)
}

// MARK: - Orders store CRUD operation errors

enum CustomersOperationError: Equatable, Error
{
  case cannotFetch(String)
  case cannotCreate(String)
  case cannotUpdate(String)
  case cannotDelete(String)
}

func ==(lhs: CustomersOperationError, rhs: CustomersOperationError) -> Bool
{
  switch (lhs, rhs) {
  case (.cannotFetch(let a), .cannotFetch(let b)) where a == b: return true
  case (.cannotCreate(let a), .cannotCreate(let b)) where a == b: return true
  case (.cannotUpdate(let a), .cannotUpdate(let b)) where a == b: return true
  case (.cannotDelete(let a), .cannotDelete(let b)) where a == b: return true
  default: return false
  }
}
