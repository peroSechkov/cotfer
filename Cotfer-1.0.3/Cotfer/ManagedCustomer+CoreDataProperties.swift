//
//  ManagedCustomer+CoreDataProperties.swift
//  Cotfer
//
//  Created by goran on 9/14/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import CoreData


extension ManagedCustomer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedCustomer> {
        return NSFetchRequest<ManagedCustomer>(entityName: "ManagedCustomer")
    }

    @NSManaged public var address: String?
    @NSManaged public var companyName: String?
    @NSManaged public var country: String?
    @NSManaged public var email: String?
    @NSManaged public var phone: String?
    @NSManaged public var role: String?
    @NSManaged public var clientId: Int32
    @NSManaged public var contacts: NSSet?

}

// MARK: Generated accessors for contacts
extension ManagedCustomer {

    @objc(addContactsObject:)
    @NSManaged public func addToContacts(_ value: ManagedContact)

    @objc(removeContactsObject:)
    @NSManaged public func removeFromContacts(_ value: ManagedContact)

    @objc(addContacts:)
    @NSManaged public func addToContacts(_ values: NSSet)

    @objc(removeContacts:)
    @NSManaged public func removeFromContacts(_ values: NSSet)

}
