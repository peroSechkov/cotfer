//
//  ResponseClientListInfo.swift
//  RVGContacts
//
//  Created by goran on 5/25/17.
//  Copyright © 2017 Darko Jovanovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseClientListInfo: Mappable {
  
  var SearchString: String?
  var OrderBy: Int?
  var Skip: Int?
  var Take: Int?
  var Total: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    SearchString <- map["SearchString"]
    OrderBy <- map["OrderBy"]
    Skip <- map["Skip"]
    Take <- map["Take"]
    Total <- map["Total"]
  }
}
