//
//  DataStoreManager.swift
//  Pickatale
//
//  Created by goran on 2/4/17.
//  Copyright © 2017 Pickatale. All rights reserved.
//

import Foundation
import CoreStore

final class DataStoreManager: NSObject {
    
    static let sharedInstance = DataStoreManager()
    
    private override init() {
        super.init();
        
        self.synthesizeDataStoreManager()
    }
    
    private static let fileName = "Cotfer"
    
    let dataStack = DataStack(xcodeModelName: fileName)
    
    private func synthesizeDataStoreManager() {
        try! self.dataStack.addStorageAndWait()
    }
}
