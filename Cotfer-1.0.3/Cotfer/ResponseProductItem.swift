import Foundation
import ObjectMapper

class ResponseProductItem:NSObject, Mappable ,NSCoding {

  var ProductCode: String?
  var ShortName: String?
  var Cat1: String?
  var Cat2: String?
  var Cat3: String?
  var Cat4: String?
  var Cat5: String?
  var Cat6: String?
  var Cat7: String?
  var Cat8: String?
  var Cat9: String?
  var Cat10: String?
  var CatDescr1: String?
  var CatDescr2: String?
  var CatDescr3: String?
  var CatDescr4: String?
  var CatDescr5: String?
  var CatDescr6: String?
  var CatDescr7: String?
  var CatDescr8: String?
  var CatDescr9: String?
  var CatDescr10: String?
  var CatSort1: String?
  var CatSort2: String?
  var CatSort3: String?
  var CatSort4: String?
  var CatSort5: String?
  var CatSort6: String?
  var CatSort7: String?
  var CatSort8: String?
  var CatSort9: String?
  var CatSort10: String?
  var Description: String?
  var Description1: String?
  var Description2: String?
  var Description3: String?
  var Description4: String?
  var Description5: String?
  var Description6: String?
  var Description7: String?
  var Description8: String?
  var Description9: String?
  var Description10: String?
  var Price: Double?
  var Price2: Double?
  var Price3: Double?
  var Price4: Double?
  var Price5: Double?
  var Price6: Double?
  var Price7: Double?
  var Price8: Double?
  var Price9: Double?
  var Price10: Double?
  var Flag1: Int?
  var Flag2: Int?
  var Flag3: Int?
  var Flag4: Int?
  var Flag5: Int?
  var Flag6: Int?
  var Flag7: Int?
  var Flag8: Int?
  var Flag9: Int?
  var Flag10: Int?
  var Flag11: Int?
  var Flag12: Int?
  var Flag13: Int?
  var Flag14: Int?
  var Flag15: Int?
  var Flag16: Int?
  var Flag17: Int?
  var Flag18: Int?
  var Flag19: Int?
  var Flag20: Int?
  var PriceQty1: Int?
  var PriceQty2: Int?
  var PriceQty3: Int?
  var PriceQty4: Int?
  var PriceQty5: Int?
  var PriceQty6: Int?
  var PriceQty7: Int?
  var PriceQty8: Int?
  var PriceQty9: Int?
  var PriceQty10: Int?
  var DescriptionNote: String?
  var UnitCode: String?
  var UnitDescr: String?
  var UnitQty: Int?
  var ItemNumber: Int?
  var Capacity: Int?
  var PricePer: Int?
  var BarCode: String?
  var ProductKind: Int?
  var ProductType: Int?
  var PopupQty: Int?
  var PriceCatalogue: Double?
  var PriceCurrency: String?
  var PriceCatalogue2: Int?
  var PriceCatalogue2Currency: String?
  var ClassProductId: Int?
  var ClassProductDescr: String?
  var TypeStratPrice: String?
  var ImageName: String?
  var Weight: Int?
  var PastSalesQty: Int?
  var WineVintage: Int?
  var WineCapacity: Int?
  var WineTaxCfdvAccountId: Int?
  var WineTaxRfdaAccountId: Int?
  var WineTaxRfdaPercent: Int?
  var ActiveFlag: Int?
  var ManageStockFlag: Int?
  var OutSelFlag: Int?
  var SNFlag: Int?
  var BatchFlag: Int?
  var LicenseFlag: Int?
  var CatalogFlag: Int?
  var AssemledFlag: Int?
  var ShopFlag: Int?
  var ImageNameCalculated: String?
  var QtyStockAvailableImmediately: Int?
  var QtyStockAvailableAtTerm: Int?
  var QtyStockAvailableAtTermDate: String?
  var ClientPrice: Double?
  var ClientPriceCurrency: String?
  var ClientTaxInc: Int?
  var ClientPriceOrigin: Int?
  var ClientPriceWDiscWoOff: Int?
  var ClientPriceWoDiscWoOff: Int?
  var IsNew: Bool?
  var ShopNumber: Int?
  var isEdited: Bool = false
  var isEditedPrice: Bool = false
  
    init(productId: String, customerPrice: Double, productTitle: String, productDescription: String, productPrice: Double, productPrice2: Double, productPrice3: Double, productPrice4: Double, productPrice5: Double, productPrice6: Double, productPrice7: Double, productPrice8: Double, productPrice9: Double, productPrice10: Double,productQty1: Int, productQty2: Int, productQty3: Int, productQty4: Int, productQty5: Int, productQty6: Int, productQty7: Int, productQty8: Int, productQty9: Int, productQty10: Int, barcode: String, unitCode: String, capacity: Int, priceCurrency: String,QtyStockAvailableImmediately : Int,QtyStockAvailableAtTerm : Int,cat1 : String,cat2 : String){
    self.ProductCode = productId
    self.ShortName = productTitle
    self.Price = productPrice
    self.Price2 = productPrice2
    self.Price3 = productPrice3
    self.Price4 = productPrice4
    self.Price5 = productPrice5
    self.Price6 = productPrice6
    self.Price7 = productPrice7
    self.Price8 = productPrice8
    self.Price9 = productPrice9
    self.Price10 = productPrice10
    self.PriceQty1 = productQty1
    self.PriceQty2 = productQty2
    self.PriceQty3 = productQty3
    self.PriceQty4 = productQty4
    self.PriceQty5 = productQty5
    self.PriceQty6 = productQty6
    self.PriceQty7 = productQty7
    self.PriceQty8 = productQty8
    self.PriceQty9 = productQty9
    self.PriceQty10 = productQty10
    self.ClientPrice = customerPrice
    self.Description = productDescription
    self.BarCode = barcode
    self.UnitCode = unitCode
    self.Capacity = capacity
    self.PriceCurrency = priceCurrency
    self.QtyStockAvailableAtTerm = QtyStockAvailableAtTerm
    self.QtyStockAvailableImmediately = QtyStockAvailableImmediately
    self.Cat1 = cat1
    self.Cat2 = cat2
  }
  
  required init?(map: Map) {}
  
  required init(coder decoder: NSCoder) {
    self.ProductCode = decoder.decodeObject(forKey: "ProductCode") as? String ?? ""
    self.ShortName = decoder.decodeObject(forKey: "ShortName") as? String ?? ""
    self.Price = decoder.decodeObject(forKey: "Price") as? Double ?? 0.0
    self.ClientPrice =  decoder.decodeObject(forKey: "ClientPrice") as? Double ?? 0.0
    self.Description =  decoder.decodeObject(forKey: "Description") as? String ?? ""
    self.BarCode =  decoder.decodeObject(forKey: "BarCode") as? String ?? ""
    self.UnitCode =  decoder.decodeObject(forKey: "UnitCode") as? String ?? ""
    self.PriceCurrency = decoder.decodeObject(forKey: "PriceCurrency") as? String ?? ""
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(ProductCode, forKey: "ProductCode")
    aCoder.encode(ShortName, forKey: "ShortName")
    aCoder.encode(Price, forKey: "Price")
    aCoder.encode(ClientPrice, forKey: "ClientPrice")
    aCoder.encode(Description, forKey: "Description")
    aCoder.encode(BarCode, forKey: "BarCode")
    aCoder.encode(UnitCode, forKey: "UnitCode")
    aCoder.encode(PriceCurrency, forKey: "PriceCurrency")
  }
  
  func mapping(map: Map) {
    ProductCode <- map["ProductCode"]
    ShortName <- map["ShortName"]
    Cat1 <- map["Cat1"]
    Cat2 <- map["Cat2"]
    Cat3 <- map["Cat3"]
    Cat4 <- map["Cat4"]
    Cat5 <- map["Cat5"]
    Cat6 <- map["Cat6"]
    Cat7 <- map["Cat7"]
    Cat8 <- map["Cat8"]
    Cat9 <- map["Cat9"]
    Cat10 <- map["Cat10"]
    CatDescr1 <- map["CatDescr1"]
    CatDescr2 <- map["CatDescr2"]
    CatDescr3 <- map["CatDescr3"]
    CatDescr4 <- map["CatDescr4"]
    CatDescr5 <- map["CatDescr5"]
    CatDescr6 <- map["CatDescr6"]
    CatDescr7 <- map["CatDescr7"]
    CatDescr8 <- map["CatDescr8"]
    CatDescr9 <- map["CatDescr9"]
    CatDescr10 <- map["CatDescr10"]
    CatSort1 <- map["CatSort1"]
    CatSort2 <- map["CatSort2"]
    CatSort3 <- map["CatSort3"]
    CatSort4 <- map["CatSort4"]
    CatSort5 <- map["CatSort5"]
    CatSort6 <- map["CatSort6"]
    CatSort7 <- map["CatSort7"]
    CatSort8 <- map["CatSort8"]
    CatSort9 <- map["CatSort9"]
    CatSort10 <- map["CatSort10"]
    Description <- map["Description"]
    Description1 <- map["Description1"]
    Description2 <- map["Description2"]
    Description3 <- map["Description3"]
    Description4 <- map["Description4"]
    Description5 <- map["Description5"]
    Description6 <- map["Description6"]
    Description7 <- map["Description7"]
    Description8 <- map["Description8"]
    Description9 <- map["Description9"]
    Description10 <- map["Description10"]
    Price <- map["Price"]
    Price2 <- map["Price2"]
    Price3 <- map["Price3"]
    Price4 <- map["Price4"]
    Price5 <- map["Price5"]
    Price6 <- map["Price6"]
    Price7 <- map["Price7"]
    Price8 <- map["Price8"]
    Price9 <- map["Price9"]
    Price10 <- map["Price10"]
    Flag1 <- map["Flag1"]
    Flag2 <- map["Flag2"]
    Flag3 <- map["Flag3"]
    Flag4 <- map["Flag4"]
    Flag5 <- map["Flag5"]
    Flag6 <- map["Flag6"]
    Flag7 <- map["Flag7"]
    Flag8 <- map["Flag8"]
    Flag9 <- map["Flag9"]
    Flag10 <- map["Flag10"]
    Flag11 <- map["Flag11"]
    Flag12 <- map["Flag12"]
    Flag13 <- map["Flag13"]
    Flag14 <- map["Flag14"]
    Flag15 <- map["Flag15"]
    Flag16 <- map["Flag16"]
    Flag17 <- map["Flag17"]
    Flag18 <- map["Flag18"]
    Flag19 <- map["Flag19"]
    Flag20 <- map["Flag20"]
    PriceQty1 <- map["PriceQty1"]
    PriceQty2 <- map["PriceQty2"]
    PriceQty3 <- map["PriceQty3"]
    PriceQty4 <- map["PriceQty4"]
    PriceQty5 <- map["PriceQty5"]
    PriceQty6 <- map["PriceQty6"]
    PriceQty7 <- map["PriceQty7"]
    PriceQty8 <- map["PriceQty8"]
    PriceQty9 <- map["PriceQty9"]
    PriceQty10 <- map["PriceQty10"]
    DescriptionNote <- map["DescriptionNote"]
    UnitCode <- map["UnitCode"]
    UnitDescr <- map["UnitDescr"]
    UnitQty <- map["UnitQty"]
    ItemNumber <- map["ItemNumber"]
    Capacity <- map["Capacity"]
    PricePer <- map["PricePer"]
    BarCode <- map["BarCode"]
    ProductKind <- map["ProductKind"]
    ProductType <- map["ProductType"]
    PopupQty <- map["PopupQty"]
    PriceCatalogue <- map["PriceCatalogue"]
    PriceCurrency <- map["PriceCurrency"]
    PriceCatalogue2 <- map["PriceCatalogue2"]
    PriceCatalogue2Currency <- map["PriceCatalogue2Currency"]
    ClassProductId <- map["ClassProductId"]
    ClassProductDescr <- map["ClassProductDescr"]
    TypeStratPrice <- map["TypeStratPrice"]
    ImageName <- map["ImageName"]
    Weight <- map["Weight"]
    PastSalesQty <- map["PastSalesQty"]
    WineVintage <- map["WineVintage"]
    WineCapacity <- map["WineCapacity"]
    WineTaxCfdvAccountId <- map["WineTaxCfdvAccountId"]
    WineTaxRfdaAccountId <- map["WineTaxRfdaAccountId"]
    WineTaxRfdaPercent <- map["WineTaxRfdaPercent"]
    ActiveFlag <- map["ActiveFlag"]
    ManageStockFlag <- map["ManageStockFlag"]
    OutSelFlag <- map["OutSelFlag"]
    SNFlag <- map["SNFlag"]
    BatchFlag <- map["BatchFlag"]
    LicenseFlag <- map["LicenseFlag"]
    CatalogFlag <- map["CatalogFlag"]
    AssemledFlag <- map["AssemledFlag"]
    ShopFlag <- map["ShopFlag"]
    ImageNameCalculated <- map["ImageNameCalculated"]
    QtyStockAvailableImmediately <- map["QtyStockAvailableImmediately"]
    QtyStockAvailableAtTerm <- map["QtyStockAvailableAtTerm"]
    QtyStockAvailableAtTermDate <- map["QtyStockAvailableAtTermDate"]
    ClientPrice <- map["Client_Price"]
    ClientPriceCurrency <- map["Client_PriceCurrency"]
    ClientTaxInc <- map["Client_TaxInc"]
    ClientPriceOrigin <- map["Client_PriceOrigin"]
    ClientPriceWDiscWoOff <- map["Client_PriceWDiscWoOff"]
    ClientPriceWoDiscWoOff <- map["Client_PriceWDiscWoOff"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}

extension ResponseProductItem {
  convenience init(managedObject: ManagedProduct) {
    self.init(productId: managedObject.productId!, customerPrice: managedObject.customerPrice, productTitle: managedObject.productTitle!, productDescription: managedObject.productDescription!, productPrice: managedObject.productPrice, productPrice2: managedObject.productPrice2, productPrice3: managedObject.productPrice3,productPrice4: managedObject.productPrice4,productPrice5: managedObject.productPrice5,productPrice6: managedObject.productPrice6,productPrice7: managedObject.productPrice7,productPrice8: managedObject.productPrice8,productPrice9: managedObject.productPrice9,productPrice10: managedObject.productPrice10,productQty1: Int(managedObject.productQty1), productQty2: Int(managedObject.productQty2), productQty3: Int(managedObject.productQty3),productQty4: Int(managedObject.productQty4),productQty5: Int(managedObject.productQty5),productQty6: Int(managedObject.productQty6),productQty7: Int(managedObject.productQty7),productQty8: Int(managedObject.productQty8),productQty9: Int(managedObject.productQty9),productQty10: Int(managedObject.productQty10),barcode: managedObject.barcode!, unitCode: managedObject.unitCode!, capacity: Int(managedObject.capacity), priceCurrency: managedObject.priceCurrency!,QtyStockAvailableImmediately : Int(managedObject.qtyStockAvailableImmediatly),QtyStockAvailableAtTerm : Int(managedObject.qtyStockAvailableAtTerm),cat1 : managedObject.category1!,cat2 : managedObject.category2!)
  }
}


