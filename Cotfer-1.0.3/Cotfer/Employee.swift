//
//  Employee.swift
//  Cotfer
//
//  Created by goran on 9/19/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class Employee: NSObject, Mappable, NSCoding {
  
  var EmployeeId: Int?
  var Code: String?
  var FirstName: String?
  var LastName: String?
  var Email: String?
  var Language: String?
  var AccessLevel: Int?
  var InactiveFlag: Int?
  var SellerId: Int?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  required init?(map: Map) {}
  
  init(employee: Employee) {
    self.EmployeeId = employee.EmployeeId
    self.Code = employee.Code
    self.FirstName = employee.FirstName
    self.LastName = employee.LastName
    self.Email = employee.Email
    self.Language = employee.Language
    self.AccessLevel = employee.AccessLevel
    self.InactiveFlag = employee.InactiveFlag
    self.IsNew = employee.IsNew
    self.ShopNumber = employee.ShopNumber
  }
  
  required init(coder decoder: NSCoder) {
    self.EmployeeId = decoder.decodeObject(forKey: "EmployeeId") as? Int ?? 0
    self.Code = decoder.decodeObject(forKey: "Code") as? String ?? ""
    self.FirstName = decoder.decodeObject(forKey: "FirstName") as? String ?? ""
    self.LastName =  decoder.decodeObject(forKey: "LastName") as? String ?? ""
    self.Email =  decoder.decodeObject(forKey: "Email") as? String ?? ""
    self.Language =  decoder.decodeObject(forKey: "Language") as? String ?? ""
    self.AccessLevel =  decoder.decodeObject(forKey: "AccessLevel") as? Int ?? 0
    self.InactiveFlag = decoder.decodeObject(forKey: "InactiveFlag") as? Int ?? 0
    self.IsNew =  decoder.decodeObject(forKey: "IsNew") as? Bool ?? false
    self.ShopNumber =  decoder.decodeObject(forKey: "ShopNumber") as? Int ?? 0
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(EmployeeId, forKey: "EmployeeId")
    aCoder.encode(Code, forKey: "Code")
    aCoder.encode(FirstName, forKey: "FirstName")
    aCoder.encode(LastName, forKey: "LastName")
    aCoder.encode(Email, forKey: "Email")
    aCoder.encode(Language, forKey: "Language")
    aCoder.encode(AccessLevel, forKey: "AccessLevel")
    aCoder.encode(InactiveFlag, forKey: "InactiveFlag")
    aCoder.encode(IsNew, forKey: "IsNew")
    aCoder.encode(ShopNumber, forKey: "ShopNumber")
  }
  
  func mapping(map: Map) {
    EmployeeId <- map["EmployeeId"]
    Code <- map["Code"]
    FirstName <- map["FirstName"]
    LastName <- map["LastName"]
    Email <- map["Email"]
    Language <- map["Language"]
    AccessLevel <- map["AccessLevel"]
    InactiveFlag <- map["InactiveFlag"]
    SellerId <- map["SellerId"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
