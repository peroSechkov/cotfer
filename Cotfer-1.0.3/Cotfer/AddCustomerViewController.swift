//
//  AddCustomerViewController.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit
import SwiftSpinner

protocol AddCustomerDisplayLogic: class
{
  func display(viewModel: AddCustomer.ViewModel)
}

class AddCustomerViewController: UIViewController, AddCustomerDisplayLogic, BaseProtocol
{
  var interactor: AddCustomerBusinessLogic?
  var router: (NSObjectProtocol & AddCustomerRoutingLogic & AddCustomerDataPassing)?
  var request = AddCustomer.Request()
  @IBOutlet weak var  companyNameTextField: UITextField!
  @IBOutlet weak var  firstNameTextField: UITextField!
  @IBOutlet weak var  lastNameTextField: UITextField!
  @IBOutlet weak var  emailAddressTextField: UITextField!
  @IBOutlet weak var  phoneNumberTextField: UITextField!
  @IBOutlet weak var  roleTextField: UITextField!
  @IBOutlet weak var saveButton: UIButton!
  var companyS: String = ""
  var lastNameS: String = ""
  var firstNameS: String = ""
  var positionS: String = ""
  var emailS: String = ""
  var phoneNumberS: String = ""
  var roles = ["Aucun", "Edition d'offres", "Atelier", "Usager", "Vendeur", "Destockage", "Gestion", "Comptabilité", "Service"]
  var pickerView :UIPickerView!
  var UserInfo: ResponseUserInfo!
  var ShopInfo: ResponseShopItem!
  
  // MARK: Object lifecycle
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?)
  {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    setup()
  }
  
  required init?(coder aDecoder: NSCoder)
  {
    super.init(coder: aDecoder)
    setup()
  }
  
  // MARK: Setup
  
  private func setup()
  {
    let viewController = self
    let interactor = AddCustomerInteractor()
    let presenter = AddCustomerPresenter()
    let router = AddCustomerRouter()
    viewController.interactor = interactor
    viewController.router = router
    interactor.presenter = presenter
    presenter.viewController = viewController
    router.viewController = viewController
    router.dataStore = interactor
  }
  
  // MARK: Routing
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?)
  {
    if let scene = segue.identifier {
      let selector = NSSelectorFromString("routeTo\(scene)WithSegue:")
      if let router = router, router.responds(to: selector) {
        router.perform(selector, with: segue)
      }
    }
  }
  
  // MARK: View lifecycle
  
  override func viewDidLoad()
  {
    super.viewDidLoad()
    createPickerView()
    setupUI()
  }
  
  func setupUI() {
    companyNameTextField.placeholder = NSLocalizedString("companyname", comment: "")
    firstNameTextField.placeholder = NSLocalizedString("firstname", comment: "")
    lastNameTextField.placeholder = NSLocalizedString("lastname", comment: "")
    emailAddressTextField.placeholder = NSLocalizedString("email", comment: "")
    phoneNumberTextField.placeholder = NSLocalizedString("phonenumber", comment: "")
    roleTextField.placeholder = NSLocalizedString("role", comment: "")
    saveButton.setTitle(NSLocalizedString("save", comment: ""), for: .normal)
  }
  
  func createPickerView(){
    pickerView = UIPickerView()
    pickerView.delegate = self
    pickerView.dataSource = self
    roleTextField.inputView = pickerView
    let toolBar = UIToolbar(frame: CGRect(x: 0, y: self.view.frame.size.height/6, width: self.view.frame.size.width, height: 40.0))
    toolBar.layer.position = CGPoint(x: self.view.frame.size.width/2, y: self.view.frame.size.height-20.0)
    toolBar.barStyle = UIBarStyle.default
    toolBar.tintColor = UIColor.white
    toolBar.backgroundColor = UIColor.black
    let doneButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.done, target: self, action: #selector(AddCustomerViewController.donePressed))
    toolBar.setItems([doneButton], animated: false)
    toolBar.isUserInteractionEnabled = false
    roleTextField.inputAccessoryView = toolBar
  }
  
  func donePressed(_ sender: UIBarButtonItem) {
    if pickerView.selectedRow(inComponent: 0) == 0
    {
      pickerView.selectRow(0, inComponent: 0, animated: true)
      self.pickerView(pickerView, didSelectRow: 0, inComponent: 0)
    }
    roleTextField.resignFirstResponder()
  }

  // MARK: Do something
  
  func addCustomer()
  {
  //  SwiftSpinner.show(NSLocalizedString("Loading", comment: ""))
    if let company = companyNameTextField.text {
      request.companyS = company
      companyS = company
    }
    if let lastName = lastNameTextField.text{
      request.lastNameS = lastName
      lastNameS = lastName
    }
    if let firstName = firstNameTextField.text
    {
      request.firstNameS = firstName
      firstNameS = firstName
    }
    if let phoneNumber = phoneNumberTextField.text
    {
      request.phoneNumberS = phoneNumber
      phoneNumberS = phoneNumber
    }
    if let email = emailAddressTextField.text
    {
      request.emailS = email
      emailS = email
    }
    if !firstNameS.isEmpty && !lastNameS.isEmpty && !positionS.isEmpty && !phoneNumberS.isEmpty && !emailS.isEmpty && !companyS.isEmpty
    {
      if (emailS.isValidEmail(emailStr: emailS))
      {
          interactor?.addCustomer(request: request)
      }
      else
      {
        showAlert(message: NSLocalizedString("Email is not valid", comment: ""))
      }
    }
    else
    {
      showAlert(message: NSLocalizedString("Please make sure First Name, Last Name, Position, phoneNumber, mobileNumber, Email are not empty.", comment: ""))
    }
  }
  
  func customerRoles()
  {
    interactor?.cusomerRoles(request: request)
  }
  
  func display(viewModel: AddCustomer.ViewModel)
  {
    SwiftSpinner.hide()
    if viewModel.viewModel != nil {
       self.navigationController?.popViewController(animated: true)
    }
  }
  
  @IBAction func saveCustomerClicked(_ sender: Any){
    addCustomer()
  }
}

extension AddCustomerViewController : UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
  func numberOfComponents(in pickerView: UIPickerView) -> Int {
    return 1
  }
  
  func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return roles.count
  }
  
  func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    return roles[row]
  }
  
  func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    roleTextField.text = roles[row]
    positionS = roles[row]
    request.positionS = positionS
  }

  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if textField == companyNameTextField {
      firstNameTextField.becomeFirstResponder()
    }else if textField == firstNameTextField {
      lastNameTextField.becomeFirstResponder()
    }else if textField == lastNameTextField {
      emailAddressTextField.becomeFirstResponder()
    }else if textField == emailAddressTextField {
      phoneNumberTextField.becomeFirstResponder()
    }else if textField == phoneNumberTextField {
      roleTextField.becomeFirstResponder()
    }
    return true
  }
  
  func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
    animateViewMoving(true, moveValue: 100)
    return true
  }
  
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    animateViewMoving(false, moveValue: 100)
    return true
  }
}
