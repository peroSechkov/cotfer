//
//  MultiProductCell.swift
//  Cotfer
//
//  Created by goran on 11/8/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import UIKit

class MultiProductCell: UITableViewCell {
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sizeValueLabel: UILabel!
    @IBOutlet weak var qtyLabel: UILabel!
    @IBOutlet weak var qtyTextField: UITextField!
    @IBOutlet weak var packageLabel: UILabel!
    @IBOutlet weak var packageValueLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var stepper: UIStepper!
    var stepperClick : ((MultiProductCell) -> Void)?
    @IBOutlet weak var lblSTD: UILabel!
    @IBOutlet weak var lblStockAtTerm: UILabel!
    override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
  }

  override func setSelected(_ selected: Bool, animated: Bool) {
      super.setSelected(selected, animated: animated)
      // Configure the view for the selected state
  }
  
  @IBAction func stepperClicked(_ sender: UIStepper) {
    self.stepperClick?(self)
  }
}
