//
//  ResponseUserInfo.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseUserInfo: NSObject, Mappable, NSCoding {
  
  var ClientNo: Int?
  var LoginNo: Int?
  var Language: String?
  var CartNo: Int?
  var EmployeeId: Int?
  var Email: String?
  var CartArticlesNo: Int?
  var CartItemsNo: Int?
  var AccessLevel: Int?
  var SellerId: Int?
  var IsNew: Bool?
  var ContactId: Int?
  var ShopNumber : Int?
  
  override init(){}
  
  required init?(map: Map) {}
  
  init(userInfo: ResponseUserInfo) {
    self.ClientNo = userInfo.ClientNo
    self.LoginNo = userInfo.LoginNo
    self.Language = userInfo.Language
    self.CartNo = userInfo.CartNo
    self.EmployeeId = userInfo.EmployeeId
    self.Email = userInfo.Email
    self.CartArticlesNo = userInfo.CartArticlesNo
    self.CartItemsNo = userInfo.CartItemsNo
    self.AccessLevel = userInfo.AccessLevel
    self.SellerId = userInfo.SellerId
    self.ContactId = userInfo.ContactId
    self.IsNew = userInfo.IsNew
    self.ShopNumber = userInfo.ShopNumber
  }

  
  required init(coder decoder: NSCoder) {
    self.ClientNo = decoder.decodeObject(forKey: "ClientNo") as? Int ?? 0
    self.LoginNo = decoder.decodeObject(forKey: "LoginNo") as? Int ?? 0
    self.Language = decoder.decodeObject(forKey: "Language") as? String ?? ""
    self.CartNo =  decoder.decodeObject(forKey: "CartNo") as? Int ?? 0
    self.EmployeeId =  decoder.decodeObject(forKey: "EmployeeId") as? Int ?? 0
    self.Email =  decoder.decodeObject(forKey: "Email") as? String ?? ""
    self.CartArticlesNo =  decoder.decodeObject(forKey: "CartArticlesNo") as? Int ?? 0
    self.CartItemsNo =  decoder.decodeObject(forKey: "CartItemsNo") as? Int ?? 0
    self.AccessLevel = decoder.decodeObject(forKey: "AccessLevel") as? Int ?? 0
    self.SellerId =  decoder.decodeObject(forKey: "SellerId") as? Int ?? 0
    self.ContactId = decoder.decodeObject(forKey: "ContactId") as? Int ?? 0
    self.IsNew =  decoder.decodeObject(forKey: "IsNew") as? Bool ?? false
    self.ShopNumber =  decoder.decodeObject(forKey: "ShopNumber") as? Int ?? 0
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(ClientNo, forKey: "ClientNo")
    aCoder.encode(LoginNo, forKey: "LoginNo")
    aCoder.encode(Language, forKey: "Language")
    aCoder.encode(CartNo, forKey: "CartNo")
    aCoder.encode(EmployeeId, forKey: "EmployeeId")
    aCoder.encode(Email, forKey: "Email")
    aCoder.encode(CartArticlesNo, forKey: "CartArticlesNo")
    aCoder.encode(CartItemsNo, forKey: "CartItemsNo")
    aCoder.encode(AccessLevel, forKey: "AccessLevel")
    aCoder.encode(SellerId, forKey: "SellerId")
    aCoder.encode(ContactId, forKey: "ContactId")
    aCoder.encode(IsNew, forKey: "IsNew")
    aCoder.encode(ShopNumber, forKey: "ShopNumber")
  }
  
  func mapping(map: Map) {
    ClientNo <- map["ClientNo"]
    LoginNo <- map["LoginNo"]
    Language <- map["Language"]
    CartNo <- map["CartNo"]
    EmployeeId <- map["EmployeeId"]
    Email <- map["Email"]
    CartArticlesNo <- map["CartArticlesNo"]
    CartItemsNo <- map["CartItemsNo"]
    SellerId <- map["SellerId"]
    ContactId <- map["ContactId"]
    AccessLevel <- map["AccessLevel"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
