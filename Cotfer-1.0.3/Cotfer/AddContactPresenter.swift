//
//  AddContactPresenter.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol AddContactPresentationLogic
{
  func present(response: AddContact.Response)
}

class AddContactPresenter: AddContactPresentationLogic
{
  weak var viewController: AddContactDisplayLogic?
  
  // MARK: Do something
  
  func present(response: AddContact.Response)
  {
    var viewModel = AddContact.ViewModel()
    viewModel.viewModel = response.response
    viewController?.display(viewModel: viewModel)
  }
}
