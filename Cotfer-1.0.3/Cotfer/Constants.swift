//
//  Constants.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation

let BASE_API_URL = "http://dev.api.radixvisualgest.com:8085/api/v01"

let USER_LOGIN_URL = "/crm/cotfer/nosection/device/login"
let EMPLOYEE_LOGIN_URL = "/crm/cotfer/nosection/employee/login"
let GET_CLIENTS_URL = "/crm/cotfer/nosection/client/list"
let ADD_CONTACT_URL = "crm/cotfer/nosection/contact/add"
let UPDATE_CONTACT_URL = "crm/cotfer/nosection/contact/set"
let GET_CONTACTS_URL = "/crm/cotfer/nosection/contact/list_byclient"
let ADD_ORDER_URL = "/crm/cotfer/nosection/cartdetails/add"
let GET_ORDER_URL = "/crm/cotfer/nosection/cartdetails/list"
let UPDATE_ORDER_URL = "/crm/cotfer/nosection/cartdetails/set"
let DELETE_ORDER_URL = "/crm/cotfer/nosection/cartdetails/del"
let EXIST_CART_ID_URL = "/crm/cotfer/nosection/cart/existingcartid"
let GET_RELATED_CLIENTS_URL = "crm/cotfer/nosection/client/list_relatedclients"
let GET_CLIENT_BY_ID_URL = "crm/cotfer/nosection/client/client"
let GET_CONTACT_BY_ID_URL = "crm/cotfer/nosection/contact/contact"
let GET_CONTACT_BY_ID_EXT_URL = "crm/cotfer/nosection/contact/contactextended"
let GET_CONTACT_BY_CLIENT_ID = "crm/cotfer/nosection/contact/list_byclient"
let SEARCH_CONTACT_URL = ""
let PRODUCT_SPEC_OFFER_URL = ""
let CATEGORY_NAMES_URL = "/crm/cotfer/nosection/product/categorynames"
let ALL_PRODUCTS_URL = "/crm/cotfer/nosection/product/list"
let GET_CATEGORY_URL = "/crm/cotfer/nosection/product/categorylist"
let PRODUCT_BY_ID_URL = ""
let STORE_SIGNATURE_URL = ""
let CUSTOMER_ROLES_URL = ""
let CONTACT_ROLES_URL = ""
let GET_DELIVERY_LIST_URL = "/crm/cotfer/nosection/client/deliverylist"
let CHECKOUT_ORDER_URL = "/crm/cotfer/nosection/cart/checkout"
let GET_CART_EXIST_URL = "/crm/cotfer/nosection/cart/existingcartid"

let SHOW_LOGIN_VC = "showLoginVC"
let SHOW_HOME_VC = "showHomeVC"
let SHOW_ADD_CONTACT_VC = "showAddContactVC"
let SHOW_ADD_CUSTOMER_VC = "showAddCustomerVC"
let SHOW_ENTER_CUSTOMER_VC = "showEnterCustomerVC"
let SHOW_PRODUCT_DETAILS_VC = "showProductDetailsVC"
let SHOW_SELECT_CATALOGUE_VC = "showSelectCatalogueVC"
let SHOW_CATALOGUE_VC = "showCatalogueVC"
let SHOW_ORDER_VC = "showOrderVC"
let SHOW_ADMIN_VC = "showAdminVC"
let SHOW_ABOUT_VC = "showAboutVC"
let SHOW_CUSTOMERS_VC = "showCustomersVC"
let SHOW_MULTI_PRODUCT_VC = "showMultiProductVC"

let OrderBy = 6
let Skip = 0
let Take = 20
let AvailableOnly = 1
let Language = "fr"
let ShopNumber = 0
let ClientNo = 0
let LoginNo = 0
let EmployeeId = 0
let Code = "vg"
let Password = "vg"
let CartNo = 0
let signatureName = "CotferSignature"
let imageExtension = ".png"
