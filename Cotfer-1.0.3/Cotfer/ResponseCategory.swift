//
//  ResponseCategory.swift
//  Cotfer
//
//  Created by goran on 9/1/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseCategory: Mappable {
  
  var Ref: String?
  var Cat: String?
  var Descr: String?
  var Num: Int?
  var Sort: String?
  var ImageName: String?
  var Description1: String?
  var IsNew: Bool?
  var ShopNumber: Int?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    Ref <- map["Ref"]
    Cat <- map["Cat"]
    Descr <- map["Descr"]
    Num <- map["Num"]
    Sort <- map["Sort"]
    ImageName <- map["ImageName"]
    Description1 <- map["Description1"]
    IsNew <- map["IsNew"]
    ShopNumber <- map["ShopNumber"]
  }
}
