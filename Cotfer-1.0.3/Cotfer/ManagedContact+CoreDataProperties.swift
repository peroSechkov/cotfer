//
//  ManagedContact+CoreDataProperties.swift
//  Cotfer
//
//  Created by goran on 9/14/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import CoreData


extension ManagedContact {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedContact> {
        return NSFetchRequest<ManagedContact>(entityName: "ManagedContact")
    }

    @NSManaged public var email: String?
    @NSManaged public var firstName: String?
    @NSManaged public var lastName: String?
    @NSManaged public var phone: String?
    @NSManaged public var role: String?
    @NSManaged public var contactId: Int32
    @NSManaged public var customer: ManagedCustomer?
    @NSManaged public var address: String?

}
