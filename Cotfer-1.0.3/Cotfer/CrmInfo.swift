//
//  CrmInfo.swift
//  RVGContacts
//
//  Created by goran on 5/25/17.
//  Copyright © 2017 Darko Jovanovski. All rights reserved.
//

import Foundation
import ObjectMapper

class CrmInfo: NSObject, Mappable, NSCoding {
    
    var Description: String?
    var SDescription: String?
    var WebSite: String?
    var SBddName: String?
    var Server: String?
    var Db: Int?
    var Language: Int?
    var DbUser: String?
    var DbPass: String?
    var IsNew: Bool?
    var ShopNumber: Int?
    
    override init(){}
    
    required init?(map: Map) {}
  
    init(crmInfo: CrmInfo) {
      self.Server = crmInfo.Server
      self.SBddName = crmInfo.SBddName
      self.DbUser = crmInfo.DbUser
      self.DbPass = crmInfo.DbPass
      self.Db = crmInfo.Db
      self.Description = crmInfo.Description
      self.SDescription = crmInfo.SDescription
      self.WebSite = crmInfo.WebSite
      self.IsNew = crmInfo.IsNew
      self.ShopNumber = crmInfo.ShopNumber
    }
  
  required init(coder decoder: NSCoder) {
    self.Server = decoder.decodeObject(forKey: "Server") as? String ?? ""
    self.SBddName = decoder.decodeObject(forKey: "SBddName") as? String ?? ""
    self.DbUser = decoder.decodeObject(forKey: "DbUser") as? String ?? ""
    self.DbPass =  decoder.decodeObject(forKey: "DbPass") as? String ?? ""
    self.Description =  decoder.decodeObject(forKey: "Description") as? String ?? ""
    self.SDescription =  decoder.decodeObject(forKey: "SDescription") as? String ?? ""
    self.WebSite =  decoder.decodeObject(forKey: "WebSite") as? String ?? ""
    self.IsNew =  decoder.decodeObject(forKey: "IsNew") as? Bool ?? false
    self.Db = decoder.decodeObject(forKey: "Db") as? Int ?? 0
    self.ShopNumber =  decoder.decodeObject(forKey: "ShopNumber") as? Int ?? 0
  }
  
  func encode(with aCoder: NSCoder) {
    aCoder.encode(Server, forKey: "Server")
    aCoder.encode(SBddName, forKey: "SBddName")
    aCoder.encode(DbUser, forKey: "DbUser")
    aCoder.encode(DbPass, forKey: "DbPass")
    aCoder.encode(Description, forKey: "Description")
    aCoder.encode(SDescription, forKey: "SDescription")
    aCoder.encode(WebSite, forKey: "WebSite")
    aCoder.encode(IsNew, forKey: "IsNew")
    aCoder.encode(Db, forKey: "Db")
    aCoder.encode(ShopNumber, forKey: "ShopNumber")
  }

    func mapping(map: Map) {
        Description <- map["Description"]
        SDescription <- map["SDescription"]
        WebSite <- map["WebSite"]
        SBddName <- map["SBddName"]
        Db <- map["Db"]
        Server <- map["Server"]
        DbUser <- map["DbUser"]
        DbPass <- map["DbPass"]
        IsNew <- map["IsNew"]
        ShopNumber <- map["ShopNumber"]        
    }
}
