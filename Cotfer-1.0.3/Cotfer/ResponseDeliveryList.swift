//
//  ResponseDeliveryList.swift
//  Cotfer
//
//  Created by goran on 9/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

struct ResponseDeliveryList: Mappable {
  
  var ClientInfo: ResponseClientInfo?
  var DeliveryClientList: [DeliveryClient]?
  var ResponseInfo: ResponseInfo?
  var ResponseUserInfo: ResponseUserInfo?
  
  init(){}
  
  init?(map: Map) {}
  
  mutating func mapping(map: Map) {
    ClientInfo <- map["ResponseData.ClientInfo"]
    DeliveryClientList <- map["ResponseData.DeliveryClientList"]
    ResponseInfo <- map["ResponseInfo"]
    ResponseUserInfo <- map["ResponseUserInfo"]
  }
}
