//
//  ProductService.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import RxSwift
import CoreStore

class ProductService: ProductStoreProtocol {
  
  func fetchStoredProducts(_ completionHandler: @escaping ([ManagedProduct], ProductsOperationError?) -> Void) {
    let products = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedProduct>()
    )
    let error: Error? = nil
    completionHandler(products!, error as? ProductsOperationError)
  }
  
  func fetchProduct(_ id: String, completionHandler: @escaping (ResponseProductItem?, ProductsOperationError?) -> Void) {
    if let product = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedProduct>()) {
    }
  }
  
  func storeProduct(_ productToStore: ResponseProductItem, completionHandler: @escaping (ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let product = transaction.create(Into<ManagedProduct>())
        product.customerPrice = productToStore.ClientPrice!
        product.productId = productToStore.ProductCode!
        product.productPrice = productToStore.Price!
        product.productTitle = productToStore.ShortName!
        product.productDescription = productToStore.Description
        product.barcode = productToStore.BarCode!
        product.capacity = Int32(productToStore.Capacity!)
        product.unitCode = productToStore.UnitCode!
    },
      completion: { _ in }
    )
  }
  
  
  func updateProduct(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        let managedProduct = transaction.fetchOne( From(ManagedProduct.self),
                                                   Where("%K == %@", "id", product.ProductCode!))
        
        managedProduct?.customerPrice = product.ClientPrice!
        managedProduct?.productDescription = product.Description
        managedProduct?.productId = product.ProductCode!
        managedProduct?.productPrice = product.Price!
        managedProduct?.productTitle = product.ShortName
    },
      completion: { _ in }
    )
  }
  
  func deleteProducts(product: ResponseProductItem, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        transaction.deleteAll( From(ManagedProduct.self) )
    },
      completion: { _ in }
    )
  }
  
  func fetchStoredImages(_ completionHandler: @escaping (_ images: [ManagedImage], _ error: ProductsOperationError?) -> Void) {
    let images = DataStoreManager.sharedInstance.dataStack.fetchAll(
      From<ManagedImage>()
    )
    let error: Error? = nil
    completionHandler(images!, error as? ProductsOperationError)
  }
  
  func fetchImage(_ id: String, completionHandler: @escaping (_ image: ManagedImage, _ error: ProductsOperationError?) -> Void) {
    if let image = DataStoreManager.sharedInstance.dataStack.fetchOne(From<ManagedImage>(),
                                                                      Where("%K == %@", "productId", id)) {
      let error: Error? = nil
      completionHandler(image, error as? ProductsOperationError)
    }
  }
  
  func storeImage(_ imageToStore: ResponseImagesInfo, completionHandler: @escaping (_ error: ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) in
        let image = transaction.create(Into<ManagedImage>())
        image.productId = imageToStore.productId
        image.picture = imageToStore.imageData! as NSData
    },
      completion: { _ in }
    )
  }
  
  func updateImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        let managedImage = transaction.fetchOne( From(ManagedImage.self),
                                                 Where("%K == %@", "productId", image.productId))
        
        if let imageId = image.productId {
          managedImage?.productId = imageId
        }
        managedImage?.picture = image.imageData as? NSData
    },
      completion: { _ in }
    )
  }
  
  func deleteImage(image: ResponseImagesInfo, completionHandler:  @escaping (_ error: ProductsOperationError?) -> Void) {
    DataStoreManager.sharedInstance.dataStack.perform(
      asynchronous: { (transaction) -> Void in
        transaction.deleteAll( From(ManagedImage.self) )
    },
      completion: { _ in }
    )
  }
  
  func getAllProducts(parameters: Parameters) -> Observable<ResponseProducts> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ProductRouter.getAllProducts(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseProducts>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
 
  func getProductById(parameters: Parameters) -> Observable<ResponseProducts> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ProductRouter.getProductById(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseProducts>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func getProductCategoryNames(parameters: Parameters) -> Observable<ResponseCategoryNames> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ProductRouter.getProductCategoryNames(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseCategoryNames>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func getProductsByCategory(parameters: Parameters) -> Observable<ResponseCategories> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(ProductRouter.getProductByCategory(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseCategories>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
            
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
  
  func getCartExist(parameters: Parameters) -> Observable<ResponseCartExist> {
    return Observable.create({ (observer) -> Disposable in
      let result = setManager().request(OrderNetRouter.getCartExist(parameters: parameters))
        .responseObject { (response: DataResponse<ResponseCartExist>) in
          switch response.result {
          case .success(let value):
            observer.onNext(value)
            observer.onCompleted()
          case .failure(let error):
            observer.onError(error)
          }
      }
      
      return Disposables.create{
        result.cancel()
      }
    })
  }
}

