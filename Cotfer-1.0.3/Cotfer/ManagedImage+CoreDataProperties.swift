//
//  ManagedImage+CoreDataProperties.swift
//  Cotfer
//
//  Created by goran on 9/20/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import CoreData


extension ManagedImage {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedImage> {
        return NSFetchRequest<ManagedImage>(entityName: "ManagedImage")
    }

    @NSManaged public var picture: NSData?
    @NSManaged public var productId: String?
    @NSManaged public var image: ManagedProduct?

}
