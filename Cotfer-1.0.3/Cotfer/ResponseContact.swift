//
//  ResponseContact.swift
//  Cotfer
//
//  Created by goran on 7/27/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseContact: NSObject, Mappable {
  
  var ShopNumber: Int?
  var ClientId: Int?
  var ContactId: Int?
  var Civility: String?
  var LastName: String?
  var FirstName: String?
  var Address: String?
  var Zip: String?
  var City: String?
  var PhoneNumber: String?
  var MobileNumber: String?
  var FaxNumber: String?
  var Email: String?
  var Position: String?
  
  init(contactId:Int, firstName:String, lastName:String, email:String, phone:String, role:String, address: String){
    self.ContactId = contactId
    self.FirstName = firstName
    self.LastName = lastName
    self.Email = email
    self.PhoneNumber = phone
    self.Address = address
    self.Position = role
  }
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    ShopNumber <- map["ShopNumber"]
    ClientId <- map["ClientId"]
    ContactId <- map["ContactId"]
    Civility <- map["Civility"]
    LastName <- map["LastName"]
    FirstName <- map["FirstName"]
    Address <- map["Address"]
    Zip <- map["Zip"]
    City <- map["City"]
    PhoneNumber <- map["PhoneNumber"]
    MobileNumber <- map["MobileNumber"]
    FaxNumber <- map["FaxNumber"]
    Email <- map["Email"]
    Position <- map["Position"]
  }
}

extension ResponseContact {
  convenience init(managedObject: ManagedContact) {
    self.init(contactId: Int(managedObject.contactId), firstName: managedObject.firstName!, lastName: managedObject.lastName! ,email: managedObject.email!, phone: managedObject.phone!, role: managedObject.role!, address: managedObject.address!)
  }
}
