//
//  ManagedOrder+CoreDataProperties.swift
//  Cotfer
//
//  Created by goran on 9/29/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import CoreData


extension ManagedOrder {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ManagedOrder> {
        return NSFetchRequest<ManagedOrder>(entityName: "ManagedOrder")
    }

    @NSManaged public var orderId: String?
    @NSManaged public var priceCurrency: String?
    @NSManaged public var productDetail: String?
    @NSManaged public var productName: String?
    @NSManaged public var productQuantity: Double
    @NSManaged public var totalPrice: Double
    @NSManaged public var lineNumber: Int32
    @NSManaged public var products: NSSet?

}

// MARK: Generated accessors for products
extension ManagedOrder {

    @objc(addProductsObject:)
    @NSManaged public func addToProducts(_ value: ManagedProduct)

    @objc(removeProductsObject:)
    @NSManaged public func removeFromProducts(_ value: ManagedProduct)

    @objc(addProducts:)
    @NSManaged public func addToProducts(_ values: NSSet)

    @objc(removeProducts:)
    @NSManaged public func removeFromProducts(_ values: NSSet)

}
