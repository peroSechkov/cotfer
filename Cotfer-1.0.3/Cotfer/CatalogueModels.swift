//
//  CatalogueModels.swift
//  Cotfer
//
//  Created by goran on 7/24/17.
//  Copyright (c) 2017 goran velkovski. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

enum Catalogue
{
  // MARK: Use cases
  struct Request
  {
    var UserInfo: ResponseUserInfo!
    var ShopInfo: ResponseShopItem!
    var Filter: String!
    var OrderBy: Int!
    var Skip: Int!
  }
  struct Response
  {
    var response: ResponseProducts!
  }
  struct ViewModel
  {
    var viewModel: ResponseProducts!
  }
}
