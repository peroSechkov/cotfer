//
//  ResponseImagesInfo.swift
//  RVGPeem
//
//  Created by goran on 6/21/17.
//  Copyright © 2017 goran velkovski. All rights reserved.
//

import Foundation
import ObjectMapper

class ResponseImagesInfo: NSObject, Mappable {
  
  var productId: String?
  var ImgPathMin: String?
  var ImgPathNormal: String?
  var ImgPathMax: String?
  var ImgPathOriginal: String?
  var ImgExtension: String?
  var imageData: Data?
  
  init(productId: String, image: Data){
    self.imageData = image
    self.productId = productId
  }
  
  required init?(map: Map) {}
  
  func mapping(map: Map) {
    ImgPathMin <- map["ImgPathMin"]
    ImgPathNormal <- map["ImgPathNormal"]
    ImgPathMax <- map["ImgPathMax"]
    ImgPathOriginal <- map["ImgPathOriginal"]
    ImgExtension <- map["ImgExtension"]
  }
}

extension ResponseImagesInfo {
  convenience init(managedObject: ManagedImage) {
    self.init(productId: managedObject.productId!, image: managedObject.picture! as Data)
  }
}
